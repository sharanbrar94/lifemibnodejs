const dotenv = require('dotenv');
dotenv.config();
const mongoose = require('mongoose');
const url =    process.env.MONGODB;
 console.log(process.env.MONGODB, `MONGODB`)
let conn = mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (!err) {
        console.log("db is connected");
    } else {
        console.log("Error in connection " + err);
    }
});

module.exports = conn;