const mongoose = require('mongoose');

const reviewImagesSchema = new mongoose.Schema({
    review_id: { type: mongoose.Schema.ObjectId, ref: 'userReviews' },
    image:{
        type: String
    }
}, { timestamps: true });

module.exports = mongoose.model('reviewImages', reviewImagesSchema);