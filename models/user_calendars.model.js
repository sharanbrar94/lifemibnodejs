const mongoose = require('mongoose');

const userCalendarSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    timezone: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true
    }
    
}, { timestamps: true });

module.exports = mongoose.model('userCalendars', userCalendarSchema);