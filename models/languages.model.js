const mongoose = require('mongoose');

const languageSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    deleted_at: {
        type: Date,
        default:""
    }
}, { timestamps: true });

module.exports = mongoose.model('languages', languageSchema);