const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    title:{
        type: String
    },
    notification:{
        type: String
    },
    notification_type:{
        type: String
    },
    status:{
        type: Number,
        default : 0
    }
    
}, { timestamps: true });

module.exports = mongoose.model('userNotifications', notificationSchema);