const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    email:{
        type: String,
        required : true
    },
    password:{
        type: String,
        required : true
    },
    phone_number:{
        type: String
    },
    profile_image:{
        type: String
    },
    first_name:{
        type: String
    },
    last_name:{
        type: String
    },
    status:{
        type: Number,
        required : true
    }
    
}, { timestamps: true });

module.exports = mongoose.model('admins', adminSchema);