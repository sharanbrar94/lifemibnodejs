const mongoose = require('mongoose');

const uwappAgentPortalSchema = new mongoose.Schema({
    user_id:{ 
        type: mongoose.Schema.ObjectId, 
        required : true 
    },
    birthday:{
        type:String,
        defalut:''
    },
    phoneNumber:{
        type:String,
        default:''
    },
    gender:{
        type:String,
        defalut:''
    },
    nicotineStatus:{
        type:String,
        defalut:''
    },
    coverageAmount:{
        type:String,
        defalut:''
    },
    selectedState:{
        type:String,
        defalut:''
    },
    height:{
        type:String,
        defalut:''
    },
    weight:{
        type:String,
        defalut:''
    },
    uwappMedicalHistoryItem:{
        type:String,
        default:''
    },
    uwappPersonalHistoryItem:{
        type:String,
        default:''
    },
    medicationArr:{
        type:String,
        default:''
    },
    appType:{
        type:String,
        defalut:''
    },
    firstName:{
        type:String,
        defalut:''
    },
    lastName:{
        type:String,
        defalut:''
    },
    email:{
        type:String,
        defalut:''
    },
    insuranceStatus:{
        type:String,
        defalut:''
    },
}, { timestamps: true })

module.exports = mongoose.model('uwappAgentPortal', uwappAgentPortalSchema)
