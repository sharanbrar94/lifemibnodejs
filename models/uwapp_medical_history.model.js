const mongoose = require('mongoose');

const uwappMedicalHistorySchema = new mongoose.Schema({
    uwappAgent_id:{ 
        type: mongoose.Schema.ObjectId, 
        ref:"uwappAgentPortal"
    },
    title:{
        type:String,
        defalut:''
    },
    status:{
        type:Number,
        defalut:''
    },
    qcat:{
        type:Number,
        defalut:''
    },
    msg:{
        type:String,
        defalut:''
    },
    qtype:{
        type:String,
        defalut:''
    },
    ptype:{
        type:String,
        defalut:''
    },
    __v:{
        type:String,
        defalut:''
    },
    answer:{
        type:String,
        defalut:''
    },
},{timestamps: true});

module.exports = mongoose.model('uwappMedicalHistory', uwappMedicalHistorySchema)