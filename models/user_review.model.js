const mongoose = require('mongoose');

const userReviewSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    email:{
        type: String,
        required : true
    },
    rating:{
        type: Number,
        default : 0
    },
    review:{
        type: String,
        default : ""
    },
    name:{
        type: String,
        default : ""
    },
    icon:{
        type: String,
        default : ""
    },
    token:{
        type: String,
        required : true
    },
    invite_token:{
        type: String,
        required : true
    }
}, { timestamps: true });

module.exports = mongoose.model('userReviews', userReviewSchema);