const mongoose = require('mongoose');

const insuranceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    deleted_at: {
        type: Date,
        default:""
    }
}, { timestamps: true });

module.exports = mongoose.model('insurances', insuranceSchema);