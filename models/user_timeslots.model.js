const mongoose = require('mongoose');

const userTimeSlotSchema = new mongoose.Schema({
    calendar_id: { type: mongoose.Schema.ObjectId, ref: 'userCalendars' },
    day:{
        type: Number,
        required : true
    },
    start_time:{
        type: String,
        required : true
    },
    end_time:{
        type: String,
        required : true
    }
    
}, { timestamps: true });

module.exports = mongoose.model('userTimeSlots', userTimeSlotSchema);