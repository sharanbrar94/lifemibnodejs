const mongoose = require('mongoose');

const customerBookingSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    date:{
        type: Date,
    },
    time:{
        type: String,
    },
    first_name:{
        type: String,
    },
    last_name:{
        type: String,
    },
    email:{
        type: String,
        required : true
    },
    dob:{
        type: Date,
    },
    phone_number:{
        type: String
    },
    insurance_type:{
        type: String
    },
    address:{
        type: String,
    },
    apartment:{
        type: String,
    },
    city:{
        type: String,
    },
    state:{
        type: String,
    },
    zipcode:{
        type: String,
    },
    message:{
        type: String,
    },
    status:{
        type: Number
    },
    booking_status:{
        type: Number,
        default:1
    }
    
}, { timestamps: true });

module.exports = mongoose.model('customerBookings', customerBookingSchema);