const mongoose = require('mongoose');

const uwappPersonalHistorySchema = new mongoose.Schema({
    uwappAgent_id:{ 
        type: mongoose.Schema.ObjectId, 
        ref:"uwappAgentPortal"
    },
    title:{
        type:String,
        defalut:''
    },
    status:{
        type:String,
        defalut:''
    },
    qcat:{
        type:String,
        defalut:''
    },
    msg:{
        type:String,
        defalut:''
    },
    qtype:{
        type:Number,
        defalut:''
    },
    ptype:{
        type:String,
        defalut:''
    },
    __v:{
        type:String,
        defalut:''
    },
    answer:{
        type:String,
        defalut:''
    },
}, {timestamps: true})

module.exports = mongoose.model('uwappPersonalHistory', uwappPersonalHistorySchema)