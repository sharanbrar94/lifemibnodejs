const mongoose = require('mongoose');

const licenseSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    deleted_at: {
        type: Date,
        default:""
    }
}, { timestamps: true });

module.exports = mongoose.model('licenses', licenseSchema);