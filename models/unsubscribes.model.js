const mongoose = require('mongoose');

const unsubscribeSchema = new mongoose.Schema({
    email:{
        type: String
    }
    
}, { timestamps: true });

module.exports = mongoose.model('unsubscribes', unsubscribeSchema);