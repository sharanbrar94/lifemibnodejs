const mongoose = require('mongoose');

const userStateSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    state_id: { type: mongoose.Schema.ObjectId, ref: 'states' }
    
}, { timestamps: true });

module.exports = mongoose.model('userStates', userStateSchema);