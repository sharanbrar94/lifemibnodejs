const mongoose = require('mongoose');

const userInsuranceSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    insurance_id: { type: mongoose.Schema.ObjectId, ref: 'insurances' }
    
}, { timestamps: true });
// }, { timestamps: true, toObject: { virtuals: true, }, toJSON: { virtuals: true } });
// userInsuranceSchema.virtual('insurance_id', {
//     ref: 'insurances',
//     localField: '_id',
//     foreignField: 'insurance_id',
//     justOne: false
// });

module.exports = mongoose.model('userInsurances', userInsuranceSchema);