const mongoose = require('mongoose');

const userSubscriptionSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    subscription_id: { type: mongoose.Schema.ObjectId, ref: 'subscriptions' },
    subscribe_id:{
        type: String,
        default : ""
    },
    price:{
        type: Number,
        default : 0
    },
    card_number:{
        type: Number,
        default : ""
    },
    brand:{
        type: String,
        default : ""
    },
    exp_month:{
        type: Number,
        default : 0
    },
    exp_year:{
        type: Number,
        default : 0
    },
    startdate:{
        type: Date,
        default : ""
    },
    enddate:{
        type: Date,
        default : ""
    },
    subscription_email:{
        type: String,
        default : ""
    },
    subscription_name:{
        type: String,
        default : ""
    },
    subscription_status:{
        type: Number,
        default : 1
    },
    next_subscription_id:{
        type: String,
        default : ""
    },
    next_subscription_name:{
        type: String,
        default : ""
    },
    next_subscription_price:{
        type: Number,
        default : 0
    },
    upgrade_at:{
        type: Date,
        default : ""
    },
    cancel_at:{
        type: Date,
        default : ""
    },
    delete_card:{
        type: Number,
        default : 0
    },
    payment_status:{
        type: Number,
        default:0

    }
    
}, { timestamps: true });

module.exports = mongoose.model('userSubscriptions', userSubscriptionSchema);