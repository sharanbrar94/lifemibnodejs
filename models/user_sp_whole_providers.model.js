const mongoose = require('mongoose');

const userSpWholeProvidersSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    // provider_id: { type: mongoose.Schema.ObjectId, ref: 'sp_providers' }
    sp_provider_id: { type: mongoose.Schema.ObjectId, required : true },
    sp_provider_title: {type: String, required : true}
    
}, { timestamps: true });

module.exports = mongoose.model('userSpWholeProviders', userSpWholeProvidersSchema);