const mongoose = require('mongoose');

const userLanguageSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    language_id: { type: mongoose.Schema.ObjectId, ref: 'languages' }
    
}, { timestamps: true });

module.exports = mongoose.model('userLanguages', userLanguageSchema);