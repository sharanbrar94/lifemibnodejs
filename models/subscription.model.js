const mongoose = require('mongoose');

const subscriptionSchema = new mongoose.Schema({
    name:{
        type: String,
        required : true
    },
    price:{
        type: Number,
        required : true
    },
    subscription_id:{
        type: String
        
    }
    
}, { timestamps: true });

module.exports = mongoose.model('subscriptions', subscriptionSchema);