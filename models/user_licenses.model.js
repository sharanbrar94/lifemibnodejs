const mongoose = require('mongoose');

const userLicenseSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    license_id: { type: mongoose.Schema.ObjectId, ref: 'licenses' }
    
}, { timestamps: true });

module.exports = mongoose.model('userLicenses', userLicenseSchema);