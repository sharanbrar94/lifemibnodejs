const mongoose = require('mongoose');

const stateSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    abbreviation: {
        type: String
    },
    deleted_at: {
        type: Date,
        default:""
    }
}, { timestamps: true });

module.exports = mongoose.model('states', stateSchema);