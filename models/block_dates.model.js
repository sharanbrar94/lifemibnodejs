const mongoose = require('mongoose');

const blockDateSchema = new mongoose.Schema({
    user_id: { type: mongoose.Schema.ObjectId, ref: 'users' },
    date:{
        type: Date,
        required : true
    }
}, { timestamps: true });

module.exports = mongoose.model('blockDates', blockDateSchema);