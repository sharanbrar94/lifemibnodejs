var mongoose = require('mongoose');
const LicenseModel = require('../models/licenses.model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            LicenseModel.create([
                { name: "Accendo Insurance Co (Aetna co.)"} ,
                { name: "Americo Financial"},
                { name: "American Amicable"} ,
                { name: "Alfa Life Insurance"},
                { name: "Allianz"} ,
                { name: "AAA Life Insurance"},
                { name: "Allstate Corp."} ,
                { name: "American Enterprise"},
                { name: "American Family Life Insurance"} ,
                { name: "American Fidelity"},
                { name: "American General Life Insurance Company (aig)"} ,
                { name: "American Home Life"},
                { name: "American National"} ,
                { name: "Ameritas Holding Company"},
                { name: "Amica Life Insurance"} ,
                { name: "Assurant Insurance Company"},
                { name: "Assurity Life Insurance Company"},
                { name: "Athene Holding Insurance Company"},
                { name: "Axa Insurance Company"},
                { name: "Boston Mutual Life"},
                { name: "Brighthouse Financial"},
                { name: "Cigna"},
                { name: "Cincinnati Life Insurance"},
                { name: "Citizens Insurance Co Of America"},
                { name: "Cno Financial Group"},
                { name: "Columbian Financial Group"},
                { name: "Continental Life Insurance (Aetna co.)"},
                { name: "Country Financial"},
                { name: "Cuna Mutual Group"},
                { name: "Cvs Health Corp."},
                { name: "Erie Family Life Insurance"},
                { name: "Family Benefit Life"},
                { name: "Federated Life Insurance"},
                { name: "Fidelity & Guaranty Life"},
                { name: "Fidelity Life Assn."},
                { name: "Foresters Financial"},
                { name: "Genworth Financial"},
                { name: "Gerber Life Insurance Company"},
                { name: "Global Atlantic"},
                { name: "Global Bankers Insurance"},
                { name: "Great Western Insurance Company"},
                { name: "Greek Catholic Union"},
                { name: "Guarantee Trust Life Heritage"},
                { name: "Guardian Insurance"},
                { name: "Horace Mann Educators Corporation"},
                { name: "Ia Financial Group"},
                { name: "Illinois Mutual Life Insurance Co"},
                { name: "Farm Bureau Insurance"},
                { name: "Iowa Farm Bureau Federation"},
                { name: "John Hancock"},
                { name: "Kansas City Life Insurance"},
                { name: "Kemper Insurance"},
                { name: "Legal & General"},
                { name: "Liberty Bankers Life"},
                { name: "Lincoln Benefit Life"},
                { name: "Lincoln Financial"},
                { name: "Lincoln Heritage Life Insurance"},
                { name: "Massmutual"},
                { name: "Metlife"},
                { name: "Michigan Farm Bureau"},
                { name: "National Guardian Life Insurance"},
                { name: "National Life Group"},
                { name: "National Western Life Grp Inc."},
                { name: "Nationwide"},
                { name: "New York Life"},
                { name: "Northwestern Mutual"},
                { name: "Ohio National"},
                { name: "Oneamerica"},
                { name: "Oxford Life Insurance Co"},
                { name: "Pacific Life"},
                { name: "Pan American Life"},
                { name: "Pekin Life Insurance"},
                { name: "Penn Mutual"},
                { name: "Physicians Mutual"},
                { name: "Primerica"},
                { name: "Principal Financial Group Inc."},
                { name: "Prosperity Life Insurance Group Llc"},
                { name: "Protective"},
                { name: "Prudential Financial Inc."},
                { name: "Riversource"},
                { name: "Royal Arcanum"},
                { name: "Royal Neighbors Of America"},
                { name: "Sammons Enterprises Inc."},
                { name: "Simply Better Life Insurance"},
                { name: "Securian Financial"},
                { name: "Security National Life Insurance Co"},
                { name: "Security National Financial"},
                { name: "Sentinel Security Life"},
                { name: "Shelter Life Insurance"},
                { name: "Sons Of Norway"},
                { name: "Southern Farm Bureau Life Insurance"},
                { name: "State Farm"},
                { name: "Sun Life Financial"},
                { name: "Symetra Financial"},
                { name: "Talcott Resolution"},
                { name: "Tennessee Farmers Life Insurance"},
                { name: "The Baltimore Life Insurance Co"},
                { name: "TIAA Financial Services"},
                { name: "Transamerica  Insurance Co"},
                { name: "United Home Life"},
                { name: "Mutual Of Omaha"},
                { name: "Unitedhealth Group"},
                { name: "Unum Insurance Company"},
                { name: "USAA"},
                { name: "Ushealth Group, Inc."},
                { name: "Voya Financial Inc."},
                { name: "Western & Southern Financial"},
                { name: "Wilton Reassurance Company, Inc."},
                { name: "Zurich Insurance Group"},
                { name: "Sagicor Financial Corporation"}
            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}