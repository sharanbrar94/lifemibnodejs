var mongoose = require('mongoose');
const AdminModal = require('../models/admin_model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            AdminModal.create([
                { email: "admin@gmail.com", password:"$2a$10$gkaYoM8nN2y/yLQyddHp/evTxgxtltCkrShFE8cbFCH6Cmk5kWXWW",status:1}
            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}