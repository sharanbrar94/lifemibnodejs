var mongoose = require('mongoose');
const StateModel = require('../models/states.model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            StateModel.create([
                { name: "ALABAMA",abbreviation:"AL"} ,
                { name: "ALASKA",abbreviation:"AK"} ,
                { name: "AMERICAN SAMOA",abbreviation:"AS"} ,
                { name: "ARIZONA",abbreviation:"AZ"} ,
                { name: "ARKANSAS",abbreviation:"AR"} ,
                { name: "CALIFORNIA",abbreviation:"CA"} ,
                { name: "COLORADO",abbreviation:"CO"} ,
                { name: "CONNECTICUT",abbreviation:"CT"} ,
                { name: "DELAWARE",abbreviation:"DE"} ,
                { name: "DISTRICT OF COLUMBIA",abbreviation:"DC"} ,
                { name: "FLORIDA",abbreviation:"FL"} ,
                { name: "GEORGIA",abbreviation:"GA"} ,
                { name: "GUAM",abbreviation:"GU"} ,
                { name: "HAWAII",abbreviation:"HI"} ,
                { name: "IDAHO",abbreviation:"ID"} ,
                { name: "ILLINOIS",abbreviation:"IL"} ,
                { name: "INDIANA",abbreviation:"IN"} ,
                { name: "IOWA",abbreviation:"IA"} ,
                { name: "KANSAS",abbreviation:"KS"} ,
                { name: "KENTUCKY",abbreviation:"KY"} ,
                { name: "LOUISIANA",abbreviation:"LA"} ,
                { name: "MAINE",abbreviation:"ME"} ,
                { name: "MARYLAND",abbreviation:"MD"} ,
                { name: "MASSACHUSETTS",abbreviation:"MA"} ,
                { name: "MICHIGAN",abbreviation:"MI"} ,
                { name: "MINNESOTA",abbreviation:"MN"} ,
                { name: "MISSISSIPPI",abbreviation:"MS"} ,
                { name: "MISSOURI",abbreviation:"MO"} ,
                { name: "MONTANA",abbreviation:"MT"} ,
                { name: "NEBRASKA",abbreviation:"NE"} ,
                { name: "NEVADA",abbreviation:"NV"} ,
                { name: "NEW HAMPSHIRE",abbreviation:"NH"} ,
                { name: "NEW JERSEY",abbreviation:"NJ"} ,
                { name: "NEW MEXICO",abbreviation:"NM"} ,
                { name: "NEW YORK",abbreviation:"NY"} ,
                { name: "NORTH CAROLINA",abbreviation:"NC"} ,
                { name: "NORTH DAKOTA",abbreviation:"ND"} ,
                { name: "NORTHERN MARIANA IS",abbreviation:"MP"} ,
                { name: "OHIO",abbreviation:"OH"} ,
                { name: "OKLAHOMA",abbreviation:"OK"} ,
                { name: "OREGON",abbreviation:"OR"} ,
                { name: "PENNSYLVANIA",abbreviation:"PA"} ,
                { name: "PUERTO RICO",abbreviation:"PR"} ,
                { name: "SOUTH CAROLINA",abbreviation:"SC"} ,
                { name: "SOUTH DAKOTA",abbreviation:"SD"} ,
                { name: "TENNESSEE",abbreviation:"TN"} ,
                { name: "TEXAS",abbreviation:"TX"} ,
                { name: "UTAH",abbreviation:"UT"} ,
                { name: "VERMONT",abbreviation:"VT"} ,
                { name: "VIRGINIA",abbreviation:"VA"} ,
                { name: "VIRGIN ISLANDS",abbreviation:"VI"} ,
                { name: "WASHINGTON",abbreviation:"WA"} ,
                { name: "WEST VIRGINIA",abbreviation:"WV"} ,
                { name: "WISCONSIN",abbreviation:"WI"} ,
                { name: "WYOMING",abbreviation:"WY"} 
            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}