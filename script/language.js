var mongoose = require('mongoose');
const LanguageModel = require('../models/languages.model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            LanguageModel.create([
                { name: "Afrikaans"} ,
                { name: "Albanian"},
                { name: "Amharic"},
                { name: "Arabic"} ,
                { name: "Armenian"},
                { name: "Azerbaijani"} ,
                { name: "Basque"},
                { name: "Belarusian"} ,
                { name: "Bengali"},
                { name: "Bosnian"} ,
                { name: "Bulgarian"},
                { name: "Catalan"} ,
                { name: "Cebuano"},
                { name: "Chinese"} ,
                { name: "Corsican"},
                { name: "Croatian"} ,
                { name: "Czech"},
                { name: "Danish"} ,
                { name: "Dutch"},
                { name: "English"} ,
                { name: "Esperanto"},
                { name: "Estonian"} ,
                { name: "Finnish"},
                { name: "French"} ,
                { name: "Frisian"},
                { name: "Galician"} ,
                { name: "Georgian"},
                { name: "German"} ,
                { name: "Greek"},
                { name: "Gujarati"} ,
                { name: "Haitian Creole"},
                { name: "Hausa"},
                { name: "Hawaiian"},
                { name: "Hebrew"},
                { name: "Hindi"},
                { name: "Hmong"},
                { name: "Hungarian"},
                { name: "Icelandic"},
                { name: "Igbo"},
                { name: "Indonesian"},
                { name: "Irish"},
                { name: "Italian"},
                { name: "Japanese"},
                { name: "Javanese"},
                { name: "Kannada"},
                { name: "Kazakh"},
                { name: "Khmer"},
                { name: "Kinyarwanda"},
                { name: "Korean"},
                { name: "Kurdish"},
                { name: "Kyrgyz"},
                { name: "Lao"},
                { name: "Latvian"},
                { name: "Lithuanian"},
                { name: "Luxembourgish"},
                { name: "Macedonian"},
                { name: "Malagasy"},
                { name: "Malay"},
                { name: "Malayalam"},
                { name: "Maltese"},
                { name: "Maori"},
                { name: "Marathi"},
                { name: "Mongolian"},
                { name: "Myanmar (Burmese)"},
                { name: "Nepali"},
                { name: "Norwegian"},
                { name: "Nyanja (Chichewa)"},
                { name: "Odia (Oriya)"},
                { name: "Pashto"},
                { name: "Persian"},
                { name: "Polish"},
                { name: "Portuguese (Portugal, Brazil)"},
                { name: "Punjabi"},
                { name: "Romanian"},
                { name: "Russian"},
                { name: "Samoan"},
                { name: "Scots Gaelic"},
                { name: "Serbian"},
                { name: "Sesotho"},
                { name: "Shona"},
                { name: "Sindhi"},
                { name: "Sinhala (Sinhalese)"},
                { name: "Slovak"},
                { name: "Slovenian"},
                { name: "Somali"},
                { name: "Spanish"},
                { name: "Sundanese"},
                { name: "Swahili"},
                { name: "Swedish"},
                { name: "Tagalog (Filipino)"},
                { name: "Tajik"},
                { name: "Tamil"},
                { name: "Tatar"},
                { name: "Telugu"},
                { name: "Thai"},
                { name: "Turkish"},
                { name: "Turkmen"},
                { name: "Ukrainian"},
                { name: "Urdu"},
                { name: "Uyghur"},
                { name: "Uzbek"},
                { name: "Vietnamese"},
                { name: "Welsh"},
                { name: "Xhosa"},
                { name: "Yiddish"},
                { name: "Yoruba"},
                { name: "Zulu"}
                
                

            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}