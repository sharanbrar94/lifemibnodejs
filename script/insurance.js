var mongoose = require('mongoose');
const InsuranceModel = require('../models/insurances.model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            InsuranceModel.create([
                { name: "Life Insurance"} ,
                { name: "Whole Life Insurance"},
                { name: "Term Insurance"},
                { name: "Mortgage Insurance"} ,
                { name: "Critical Illness Insurance"},
                { name: "Final Expense Insurance"},
                { name: "Universal Life Insurance"} ,
                { name: "Guaranteed life insurance"},
                { name: "Indexed Universal Life"} ,
                { name: "Long-term Care Insurance"},
                { name: "Medicare Advantage"},
                { name: "Medicare Part A, B, C and D"} ,
                { name: "Medicare Supplement"},
                { name: "Retirement Planning"},
                { name: "Variable Universal Life Insurance"} ,
                { name: "Accidental Death And Dismemberment (ad&d)"},
                { name: "Annuities"},
                { name: "Financial Planning"} ,
                { name: "Income Tax Planning Strategies"},
                { name: "Group Health Insurance"},
                { name: "Health Insurance"} ,
            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}