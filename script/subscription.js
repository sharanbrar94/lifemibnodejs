var mongoose = require('mongoose');
const SubscriptionModal = require('../models/subscription.model');
const async = require('async');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/lifemib', {
    useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true
}, () => {
    console.log('you are connected to MongoDb');
    insertAds();
});
mongoose.connection.on('error', (err) => {
    console.log('Mongodb connection failed due to error : ', err);
});
function insertAds() {
    async.waterfall([
        function (callback) {
            SubscriptionModal.create([
                { name: "Free", price:0,subscription_id:""} ,
                { name: "Premium Profile",price:49,subscription_id:"price_1JZaQwJ1M02eHZqySWDnrZgO"},
                { name: "Life Essential" , price:99,subscription_id:"price_1JZaRYJ1M02eHZqyWsEnYFWJ"},
                { name: "Life Unlimited",price:299,subscription_id:"price_1JZaSiJ1M02eHZqyD7eFpKb3"}
            ], function (err, data) {                
                if (err) {
                    console.log("Error in inserting Ads.", data);
                    process.exit();
                }
                else { callback(null, data) }
            });
        },], function (err, data) {
            console.log("successfully save Ads .");
            process.exit();
        });
}