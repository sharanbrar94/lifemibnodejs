const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
var path = require('path')
//attach the plugin to the nodemailer transporter
console.log(path.join(__dirname, "../public/views/email-verification.hbs"))
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'support@lifemib.com',
    pass: 'cqffavjzfcybowvi'
  }
});
const options = {
  viewEngine: {
    partialsDir: path.join(__dirname, "../public/views/partials"),
    layoutsDir: path.join(__dirname, "../public/views/layouts"),
    extname: ".hbs"
  },
  extName: ".hbs",
  viewPath: ""
};
transporter.use("compile", hbs(options));

class Common {
  static async pushEmail(email,subject,content) {
    try {
      var mailOptions = {

        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/forgot-pass"),
        context: {
          // Data to be sent to template engine.
          //url: 'https://henceforthsolutions.com/',
          text: content,
          email:email,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }
  static async sendMail(email, subject, text,otp,getUrl) {
    try {
      var mailOptions = {

        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/forgot-pass"),
        context: {
          // Data to be sent to template engine.
          //url: 'https://henceforthsolutions.com/',
          text: text,
          otp:otp,
          email:email,
          getUrl:getUrl,
          subject: subject,
        }
      }

      let mail = await transporter.sendMail(mailOptions)
      console.log(mail);

    } catch (err) {
      console.log(err);
      throw err;
    }

    return "success"
  }

  /*=========review email======*/

  static async reviewEmail(email, subject,link,image,name,content,getUrl) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/index"),
        context: {
          link:link,
          image:image,
          name:name,
          content:content,
          email:email,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }

  /*=========welcome email========*/
  static async welcomeEmail(email,subject,getUrl) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/welcome"),
        context: {
          name:email,
          email:email,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*=========appointment email========*/
  static async booking(email,subject,first_name,time,date,getUrl) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/booking"),
        context: {
          name:first_name,
          time:time,
          email:email,
          date:date,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*=========new review email========*/
  static async newReview(email,subject,name,review,getUrl) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/new_review"),
        context: {
          email:email,
          name:name,
          review:review,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*=========email to user regarding review========*/
  static async reviewUserEmail(email,subject,token,getUrl,agentName) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/review-user"),
        context: {
          email:email,
          token:token,
          agentName:agentName,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*=========subscription email========*/
  static async subscription(email,subject,name,getUrl) {
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/subscription"),
        context: {
          email:email,
          name:name,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*========Invite email=======*/
  static async inviteEmail(email, subject, customerName, agentName,agent_id,getUrl){
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/invite"),
        context: {
          email:email,
          customerName:customerName,
          agentName:agentName,
          agent_id:agent_id,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  /*======cancel appointment======*/
  static async cancelAppointment(email, subject, customerName, date,time,getUrl){
    try {
      var mailOptions = {
        from: 'support@lifemib.com',
        to: email,
        subject: subject,
        template: path.join(__dirname, "../public/email/cancel"),
        context: {
          email:email,
          customerName:customerName,
          date:date,
          time:time,
          getUrl:getUrl,
          subject: subject
          
        }
      }
      await transporter.sendMail(mailOptions)
    } 
    catch (err) {
      console.log(err);
      throw err;
    }
    return "success"
  }
  
  
  //Generate Random String
  static async generateRandomString(length) {
    var text = '';
    const possible =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }

  
  
  static async generateRandomNumber(length) {
    var text = "";
    const possible = "123456789";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    var random = parseInt(text);
    return random;
  }

 
  /*=====user data=====*/
  static async userData(user_id){
    let users = `select * from users where id=?`
    const getUser = await connection.query(users, [user_id])
    return getUser[0]
  }
  /*======get booking review======*/
  static async bookingReviews(booking_id){
    let bookingReviews = `select booking_reviews.*,b.booking_unique_id,p.title,p.id as post_id,fu.first_name as student_first_name,fu.last_name as student_last_name,fu.profile_image as student_profile_image,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image from booking_reviews left join users as fu on booking_reviews.from_user_id=fu.id left join users as tu on booking_reviews.to_user_id=tu.id left join booking_requests as b on booking_reviews.booking_id = b.id left join posts as p on b.post_id = p.id where booking_reviews.booking_id=?`
    const getReview = await connection.query(bookingReviews, [booking_id])
    return getReview[0]
  }
  static async reviewById(review_id){
    let bookingReviews = `select booking_reviews.*,fu.first_name as student_first_name,fu.last_name as student_last_name,fu.profile_image as student_profile_image,tu.first_name as teacher_first_name,tu.last_name as teacher_last_name,tu.profile_image as teacher_profile_image from booking_reviews left join users as fu on booking_reviews.from_user_id=fu.id left join users as tu on booking_reviews.to_user_id=tu.id where booking_reviews.id=?`
    const getReview = await connection.query(bookingReviews, [review_id])
    return getReview[0]
  }
  /*=======get currency detail=====*/
  static async getCurrencyRate(from,to,amount){
    if(from == 'USD'){
      let getRate = await this.getExchangeRate(from,to)
      var getAmount = getRate*amount
    }
    if(from == 'JPY' ){
      if(to == 'USD'){
        let getRate = await this.getExchangeRate('USD','JPY')
        var getAmount = (1/getRate)*amount
      }
      if(to == 'SGD'){
        let jpyUsd = await this.getExchangeRate('USD','JPY')
        let usdSgd = await this.getExchangeRate('USD','SGD')
        let jpyUsdAmount = (1/jpyUsd)
        let usdSgdAmount = usdSgd
        var getAmount = (jpyUsdAmount*usdSgdAmount)*amount
      }
    }
    if(from == 'SGD' ){
      if(to == 'USD'){
        let getRate = await this.getExchangeRate('USD','SGD')
        var getAmount = (1/getRate)*amount
      }
      if(to == 'JPY'){
        let sgdUsd = await this.getExchangeRate('USD','SGD')
        let usdJpy = await this.getExchangeRate('USD','JPY')
        let sgdUsdAmount = (1/sgdUsd)
        let usdJpyAmount = usdJpy
        var getAmount = (sgdUsdAmount*usdJpyAmount)*amount
      }
    }
    return getAmount

  }
  static async getExchangeRate(from,to){
    let query = `select * from exchange_rates where from_currency = '${from}' and to_currency = '${to}'`
    let exchangeRate = await connection.query(query)
    return exchangeRate[0].rate
  }

  /*=======call localization file according to language======*/

  static dbColumnLang(col_name,lang){
    return lang!="en"?col_name+"_"+lang:col_name
  }

  static async localization(lang){
    
    if(lang == "ar"){
      var localFile = require("./localization-jp.js");
      var getMessage = await localFile.lang();
    }
    else{
      var localFile = require("./localization-en.js");
      var getMessage = await localFile.lang();
    }
    return getMessage
  }
  /*=====stripe add bank details=====*/
//  static async createBankAccount(name, routing_number, account_number) {
//     var response = await this.Stripe.tokens.create(
//       {
//         bank_account: {
//           country: 'JP',
//           currency: 'JPY',
//           account_holder_name: name,
//           account_holder_type: 'individual',
//           routing_number:  1100000,
//           account_number: 00012345,
//         },
//       }
//     );
//     console.log('create bank', response)    
//     return response  
//   }
//   static async createStripeCustomAccount(name, last_name, ssn, state, town, street_address, zip_code, bank_account_id, email, date, month, year, slug, phone, stripe_front_id, stripe_back_id) {
//     let response = await this.Stripe.accounts.create(
//       {
//         type: 'custom',
//         country: 'JP',
//         email: email,
//         capabilities: {
//           card_payments: { requested: true },
//           transfers: { requested: true },
//         },
//         business_type: 'individual',
//         business_profile: {
//           //url: 'potswork.com',
//           url: 'https://www.potswork.com/provider-profile/' + slug,
//           product_description: '',
//           name: email,
//           support_email: email,
//           support_phone: phone,
//           support_url: 'https://www.potswork.com/provider-profile/' + slug,
//           support_address: {
//             city: town,
//             country: 'JP',
//             line1: street_address,
//             line2: street_address,
//             postal_code: zip_code,
//             state: state
//           },
//           mcc: '8999'        
//         },
//         individual: {
//           address: {
//             city: town,
//             country: 'US',
//             line1: street_address,
//             line2: street_address,
//             postal_code: zip_code,
//             state: state
//           },
//           dob: {
//             day: date,
//             month: month,
//             year: year
//           },
//           ssn_last_4: (ssn_num.slice(ssn_num.length - 4)),
//           first_name: name, last_name: last_name,
//           phone: phone,
//           email: email,
//           id_number: ssn_num,
//           verification: {
//             document: {
//               back: stripe_back_id ? stripe_back_id : null,
//               front: stripe_front_id ? stripe_front_id : null
//             }
//           }
//         },        tos_acceptance: {
//           date: 1574863976,
//           ip: '73.85.204.126'
//         },
//         external_account: bank_account_id,      
//       })    
//       return response
//   }
}
module.exports = Common;