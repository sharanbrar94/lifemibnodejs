require('dotenv').config();
const logger = require('../../logger') // importiing winston logger module
const jwt = require('jsonwebtoken')
const AdminService = require("./admin.service");
const path = require("path")
class AdminController {
  static async login(req, res, next) {
    try {
      let email = req.body.email;
      let password = req.body.password
      let user = await AdminService.login(email, password);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async loginAsUser(req, res, next){
    try {
      let id = req.query.id;
      let token = req.query.token;
      let user = await AdminService.loginAsUser(id,token);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getAllAgents(req, res, next){
    try {
      let user = await AdminService.getAllAgents();
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getProfile(req, res, next) {
    try {
      let user_id = req.user._id;
      let user = await AdminService.getProfile(user_id)
      return res.status(200).json(user)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async updateProfile(req, res, next) {
    try {
      let first_name = req.body.first_name;
      let last_name = req.body.last_name;
      let profile_image = req.body.profile_image;
      let phone_number = req.body.phone_number;
      let logged_id = req.user._id
      const result = await AdminService.updateProfile(logged_id, first_name, last_name, profile_image,phone_number)
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async changePassword(req, res, next) {
    try {
      let logged_id = req.body.id;
      let old_password = req.body.old_password;
      let new_password = req.body.new_password;
      console.log(logged_id)
      const result = await AdminService.changePassword(new_password, old_password, logged_id);
      return res.status(200).json(result)
    } catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async sendNotification(req, res, next) {
    try {
      let user_id = req.user._id
      let email = req.body.emails;
      let subject = req.body.subject;
      let email_content = req.body.email_content;
      await AdminService.sendMail(email, subject, email_content);
      return res.status(200).json({
        message: 'Mail Sent Successfully'
      })
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //language module
  static async getLanguages(req, res, next) {
    try {
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let languages = await AdminService.getLanguages(page, limit,keyword);
      return res.status(200).json(languages)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addLanguage(req, res, next) {
    try {
      let name = req.body.name;
      let user = await AdminService.addLanguage(name);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getLanguage(req, res, next){
    try {
      let id = req.query.id;
      let lang = await AdminService.getLanguage(id);
      return res.status(200).json(lang)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editLanguage(req, res, next) {
    try {
      let id = req.body.id;
      let name = req.body.name
      let user = await AdminService.editLanguage(id, name);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteLanguage(req, res, next){
    try {
      let id = req.body.id;
      let user = await AdminService.deleteLanguage(id);
      return res.status(200).json(user)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //insurance module
  static async getInsurances(req, res, next) {
    try {
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let insurances = await AdminService.getInsurances(page, limit,keyword);
      return res.status(200).json(insurances)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addInsurance(req, res, next) {
    try {
      let name = req.body.name;
      let insurance = await AdminService.addInsurance(name);
      return res.status(200).json(insurance)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getInsurance(req, res, next){
    try {
      let id = req.query.id;
      let insurance = await AdminService.getInsurance(id);
      return res.status(200).json(insurance)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editInsurance(req, res, next) {
    try {
      let id = req.body.id;
      let name = req.body.name
      let insurance = await AdminService.editInsurance(id, name);
      return res.status(200).json(insurance)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteInsurance(req, res, next){
    try {
      let id = req.body.id;
      let insurance = await AdminService.deleteInsurance(id);
      return res.status(200).json(insurance)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //license module
  static async getLicenses(req, res, next) {
    try {
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let licenses = await AdminService.getLicenses(page, limit,keyword);
      return res.status(200).json(licenses)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addLicense(req, res, next) {
    try {
      let name = req.body.name;
      let license = await AdminService.addLicense(name);
      return res.status(200).json(license)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getLicense(req, res, next){
    try {
      let id = req.query.id;
      let license = await AdminService.getLicense(id);
      return res.status(200).json(license)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editLicense(req, res, next) {
    try {
      let id = req.body.id;
      let name = req.body.name
      let license = await AdminService.editLicense(id, name);
      return res.status(200).json(license)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteLicense(req, res, next){
    try {
      let id = req.body.id;
      let license = await AdminService.deleteLicense(id);
      return res.status(200).json(license)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  
  //state module
  static async getStates(req, res, next) {
    try {
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let states = await AdminService.getStates(page, limit,keyword);
      return res.status(200).json(states)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async addState(req, res, next) {
    try {
      let name = req.body.name;
      let state = await AdminService.addState(name);
      return res.status(200).json(state)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async getState(req, res, next){
    try {
      let id = req.query.id;
      let state = await AdminService.getState(id);
      return res.status(200).json(state)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async editState(req, res, next) {
    try {
      let id = req.body.id;
      let name = req.body.name
      let state = await AdminService.editState(id, name);
      return res.status(200).json(state)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async deleteState(req, res, next){
    try {
      let id = req.body.id;
      let state = await AdminService.deleteState(id);
      return res.status(200).json(state)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  //agent module
  static async getAgents(req, res, next){
    try {
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let agents = await AdminService.getAgents(page,limit,keyword);
      return res.status(200).json(agents)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async agentDetail(req, res, next){
    try {
      let id = req.query.id;
      
      let agent = await AdminService.agentDetail(id);
      return res.status(200).json(agent)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async actDeactUser(req, res, next) {
    try {
      let id = req.body.id;
      let deactivate = req.body.deactivate;
      const response = await AdminService.actDeactUser(id,deactivate);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async agentVerified(req, res, next){
    try {
      let id = req.body.id;
      let verified = req.body.verified;
      const response = await AdminService.agentVerified(id,verified);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  static async agentDelete(req, res, next) {
    try {
      let id = req.body.id;
      const response = await AdminService.deleteAgent(id);
      return res.status(200).json(response)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //review module
  static async getReviews(req, res, next){
    try {
      let id = req.query.id;
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let agent = await AdminService.getReviews(id,page,limit,keyword);
      return res.status(200).json(agent)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //subscription module
  static async getSubscriptions(req, res, next){
    try {
      let id = req.query.id;
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let agent = await AdminService.getSubscriptions(id,page,limit,keyword);
      return res.status(200).json(agent)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //booking module
  static async getBookings(req, res, next){
    try {
      let id = req.query.id;
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      console.log("test")
      let agent = await AdminService.getBookings(id,page,limit,keyword);
      return res.status(200).json(agent)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }
  //notification module
  static async getNotifications(req, res, next){
    try {
      let id = req.query.id;
      let page = req.query.page;
      let limit = req.query.limit;
      let keyword = req.query.keyword;
      let agent = await AdminService.getNotifications(id,page,limit,keyword);
      return res.status(200).json(agent)
    }
    catch (err) {
      logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
      return res.status(400).json({ 'error': "bad_request", 'error_description': err });
    }
  }

  
}
module.exports = AdminController;