var express = require('express');
var router = express.Router();
const AdminController = require("./admin.controller");
let auth = require("../../middleware/auth")


router.post('/login',AdminController.login);
router.get('/profile', auth.checkToken, AdminController.getProfile);
router.put('/profile', auth.checkToken, AdminController.updateProfile);
router.put('/change-password', auth.checkToken,AdminController.changePassword);
router.post('/notification', auth.checkToken, AdminController.sendNotification);
router.get('/login-as-user',auth.checkToken, AdminController.loginAsUser);
router.get('/all-agents',auth.checkToken, AdminController.getAllAgents);
//language module
router.get('/languages',auth.checkToken,AdminController.getLanguages);
router.post('/language',auth.checkToken,AdminController.addLanguage);
router.put('/language',auth.checkToken,AdminController.editLanguage);
router.get('/language',auth.checkToken,AdminController.getLanguage);
router.post('/delete-language',auth.checkToken,AdminController.deleteLanguage);
//insurances module
router.get('/insurances',auth.checkToken,AdminController.getInsurances);
router.post('/insurance',auth.checkToken,AdminController.addInsurance);
router.put('/insurance',auth.checkToken,AdminController.editInsurance);
router.get('/insurance',auth.checkToken,AdminController.getInsurance);
router.post('/delete-insurance',auth.checkToken,AdminController.deleteInsurance);
//licenses module
router.get('/licenses',auth.checkToken,AdminController.getLicenses);
router.post('/license',auth.checkToken,AdminController.addLicense);
router.put('/license',auth.checkToken,AdminController.editLicense);
router.get('/license',auth.checkToken,AdminController.getLicense);
router.post('/delete-license',auth.checkToken,AdminController.deleteLicense);
//states module
router.get('/states',auth.checkToken,AdminController.getStates);
router.post('/state',auth.checkToken,AdminController.addState);
router.put('/state',auth.checkToken,AdminController.editState);
router.get('/state',auth.checkToken,AdminController.getState);
router.post('/delete-state',auth.checkToken,AdminController.deleteState);
//agent module
router.get('/agents',auth.checkToken,AdminController.getAgents);
router.get('/agent-detail',auth.checkToken,AdminController.agentDetail);
router.post('/agent-delete', auth.checkToken, AdminController.agentDelete);
router.post('/deactivate', auth.checkToken, AdminController.actDeactUser);
router.post('/verified', auth.checkToken, AdminController.agentVerified);
//review module
router.get('/reviews',auth.checkToken,AdminController.getReviews);
//subscription module
router.get('/subscriptions',auth.checkToken,AdminController.getSubscriptions);
//booking module
router.get('/bookings',auth.checkToken,AdminController.getBookings);
//notification module
router.get('/notifications',auth.checkToken,AdminController.getNotifications);



module.exports = router;