const { sign } = require('jsonwebtoken');
let Common = require("../../middleware/common")
const AdminModel = require('../../models/admin_model');
const userSchema = require('../users/model/users.model');
const LanguageModel = require('../../models/languages.model');
const LicenseModel = require('../../models/licenses.model');
const StateModel = require('../../models/states.model');
const InsuranceModel = require('../../models/insurances.model');
const UserInsuranceModel = require('../../models/user_insurances.model');
const UserLanguageModel = require('../../models/user_languages.model');
const UserLicenseModel = require('../../models/user_licenses.model');
const UserStateModel = require('../../models/user_states.model');
const UserCalendarModel = require('../../models/user_calendars.model');
const UserTimeslotModel = require('../../models/user_timeslots.model');
const SubscriptionModal = require('../../models/subscription.model');
const UserSubscriptionModal = require('../../models/user_subscription.model');
const ReviewModal = require('../../models/user_review.model');
const CustomerBookingModal = require('../../models/customer_bookings.model');
const ReviewImageModal = require('../../models/review_images.model');
const BlockDatesModal = require('../../models/block_dates.model');
const UnsubscribeModal = require('../../models/unsubscribes.model');
const NotificationSchema = require('../../models/notifications.model');
const UserService = require("../users/users.service");
const { genSaltSync, hashSync, compareSync } = require("bcryptjs");
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
class AdminService {
    static async login(email, password) {
        let getUser = await this.getAdminDetail(email);
        if (getUser) {
            let passwordResult = compareSync(password, getUser.password);
            if (!passwordResult) {
                throw "Please enter correct password";
            }
        }
        else {
            throw "No account found with this email";
        }
        let auth_token = await this.generateToken(getUser);
        let userId = getUser._id
        let user = await this.getUserProfile(userId)
        return { message: "Login successfully", token: auth_token, ...user }
    }
    static async loginAsUser(id,token){
        let user = await UserService.getUserProfile(id)
        let auth_token = await UserService.generateToken(user);
        return ({ 'message': "User login successfully", 'token': auth_token, ...user });
    }
    static async getAllAgents(){
        let user = await userSchema.aggregate([
            // {
            //     $match: { _id: ObjectId(id) }
            // },
            {
                "$project": {
                    "_id": 1, "email": 1
                }
            }
        ]);
        return user
    }
    static async getProfile(id) {
        return await this.getUserProfile(id)
    }
    static async updateProfile(logged_id, first_name, last_name, profile_image,phone_number) {
        
        let data = {}
        if (first_name != undefined) {
            data.first_name = first_name
        }
        if (last_name != undefined) {
            data.last_name = last_name
        }
        if (profile_image != undefined) {
            data.profile_image = profile_image
        }
        if (phone_number != undefined) {
            data.phone_number = phone_number
        }
        await AdminModel.findOneAndUpdate({ "_id": logged_id }, { $set: data }, { new: true, upset: true });
        let admin = await this.getUserProfile(logged_id)
        return { message: "Profile updated successfully",admin }
    }
    static async getAdminDetail(email) {
        return await AdminModel.findOne({ email: email });
    }
    static async generateToken(user) {
        let token = await sign({ user: user }, 'wLJhp7xEMd772mqUfxZhZaDN8Uy7JrQR', { expiresIn: "2 Days" });
        return token
    }
    static async getUserProfile(id) {
        let user = await AdminModel.aggregate([
            {
                $match: { _id: ObjectId(id) }
            },
            {
                "$project": {
                    "_id": 1, "email": 1,"phone_number":1,"profile_image":1,"first_name":1,"last_name":1,"status": 1
                }
            }

        ]);
        return user[0]
    }
    static async changePassword(new_password, old_password, logged_id) {
        const user = await AdminModel.findOne({ _id: logged_id });
        console.log("profile",user)
        let result = compareSync(old_password, user.password);
        if (!result) {
            throw ('Please enter correct old password.');
        } else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            let data = {}
            data.password = password
            let sqlQuery = await AdminModel.findOneAndUpdate({ "_id": logged_id }, { $set: data }, { new: true, upset: true });
            return ({ message: "Password changed" });
        }
    }
    static async sendMail(email, subject, content) {
        if (email) {
            email.forEach(async x=>{
                await Common.pushEmail(data,subject,content)
            })
            
        } 
        else {
            throw("Error while sending email...")
        }
    }
    //language module
    static async getLanguages(page, limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let wrkObj = {}, arr = [];
        arr.push({ __v:{ $ne: 1 }})
        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        wrkObj = { $and: arr }
        let getLanguages = await LanguageModel.aggregate([
            {$match: wrkObj },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let languages = []
        let count = 0
        if (getLanguages[0].data.length > 0) {
            languages = getLanguages[0].data
            count = getLanguages[0].metadata[0].total
        }
        return ({ count: count, languages })
    }
    static async addLanguage(name){
        const getLang = await LanguageModel.find({ name: name });
        if(getLang.length > 0){
            throw("Language already added")
        }
        else{
            const addlanguage = new LanguageModel({ "name":name});
            const addLang = await addlanguage.save();
            let lastInsertedId = addLang.id
            let language = await this.getLanguage(lastInsertedId)
            return({message:"Language added",language})
        }
    }
    static async getLanguage(id){
        const language = await LanguageModel.findOne({ "_id":id });
        return language
    }
    static async editLanguage(id,name){
        let data = {}
        data.name  = name
        await LanguageModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let language = await this.getLanguage(id)
        return({message:"Language updated",language})
    }
    static async deleteLanguage(id){
        let data = {}
        data.__v  = 1
        await LanguageModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        return({message:"Language deleted"})
    }
    //insuarnace module
    static async getInsurances(page, limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let wrkObj = {}, arr = [];
        arr.push({ __v:{ $ne: 1 }})
        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        wrkObj = { $and: arr }
        let getInsurances = await InsuranceModel.aggregate([
            {$match: wrkObj },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let insurances = []
        let count = 0
        if (getInsurances[0].data.length > 0) {
            insurances = getInsurances[0].data
            count = getInsurances[0].metadata[0].total
        }
        return ({ count: count, insurances })
    }
    static async addInsurance(name){
        const getInsu = await InsuranceModel.find({ name: name });
        if(getInsu.length > 0){
            throw("Insurance already added")
        }
        else{
            const addInsurance = new InsuranceModel({ "name":name});
            const addInsu = await addInsurance.save();
            let lastInsertedId = addInsu.id
            let insurance = await this.getInsurance(lastInsertedId)
            return({message:"Insurance added",insurance})
        }
    }
    static async getInsurance(id){
        const insurance = await InsuranceModel.findOne({ "_id":id });
        return insurance
    }
    static async editInsurance(id,name){
        let data = {}
        data.name  = name
        await InsuranceModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let insurance = await this.getInsurance(id)
        return({message:"Insurance updated",insurance})
    }
    static async deleteInsurance(id){
        let data = {}
        data.__v  = 1
        await InsuranceModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        return({message:"Insurance deleted"})
    }
    //license module
    static async getLicenses(page, limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let wrkObj = {}, arr = [];
        arr.push({ __v:{ $ne: 1 }})
        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        wrkObj = { $and: arr }
        let getLicenses = await LicenseModel.aggregate([
            {$match: wrkObj },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let licenses = []
        let count = 0
        if (getLicenses[0].data.length > 0) {
            licenses = getLicenses[0].data
            count = getLicenses[0].metadata[0].total
        }
        return ({ count: count, licenses })
    }
    static async addLicense(name){
        const getLic = await LicenseModel.find({ name: name });
        if(getLic.length > 0){
            throw("License already added")
        }
        else{
            const addLicense = new LicenseModel({ "name":name});
            const addLic = await addLicense.save();
            let lastInsertedId = addLic.id
            let license = await this.getLicense(lastInsertedId)
            return({message:"License added",license})
        }
    }
    static async getLicense(id){
        const license = await LicenseModel.findOne({ "_id":id });
        return license
    }
    static async editLicense(id,name){
        let data = {}
        data.name  = name
        await LicenseModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let license = await this.getLicense(id)
        return({message:"License updated",license})
    }
    static async deleteLicense(id){
        let data = {}
        data.__v  = 1
        await LicenseModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        return({message:"License deleted"})
    }
    //state module
    static async getStates(page,limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let wrkObj = {}, arr = [];
        arr.push({ __v:{ $ne: 1 }})
        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        wrkObj = { $and: arr }
        let getStates = await StateModel.aggregate([
            {$match:wrkObj },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let states = []
        let count = 0
        if (getStates[0].data.length > 0) {
            states = getStates[0].data
            count = getStates[0].metadata[0].total
        }
        return ({ count: count, states })
    }
    static async addState(name){
        const getSta = await StateModel.find({ name: name });
        if(getSta.length > 0){
            throw("State already added")
        }
        else{
            const addState = new StateModel({ "name":name});
            const addSta = await addState.save();
            let lastInsertedId = addSta.id
            let state = await this.getState(lastInsertedId)
            return({message:"License added",state})
        }
    }
    static async getState(id){
        const license = await StateModel.findOne({ "_id":id });
        return license
    }
    static async editState(id,name){
        let data = {}
        data.name  = name
        await StateModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let state = await this.getState(id)
        return({message:"State updated",state})
    }
    static async deleteState(id){
        let data = {}
        data.__v  = 1
        await StateModel.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        return({message:"State deleted"})
    }
    static async getAgents(page,limit,keyword){
        var match = {}
        let wrkObj = {}, arr = [];
        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        else {
            arr.push({ "_id": { "$exists": true } })
        }
        wrkObj = { $and: arr }
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let aggregationCriteria = [
            { $match: match },
            {
                "$lookup": {
                    from: "userinsurances",
                    localField: "_id",
                    foreignField: "user_id",
                    as: "userInsurance"
                }
            },
            { "$lookup": { from: "insurances", localField: "userInsurance.insurance_id", foreignField: "_id", as: "userInsurance.insurances" } },

            { "$lookup": { from: "userlicenses", localField: "_id", foreignField: "user_id", as: "userLicense" } },
            { "$lookup": { from: "licenses", localField: "userLicense.license_id", foreignField: "_id", as: "userLicense.licenses" } },

            { "$lookup": { from: "userstates", localField: "_id", foreignField: "user_id", as: "userState" } },
            { "$lookup": { from: "states", localField: "userState.state_id", foreignField: "_id", as: "userState.states" } },

            { "$lookup": { from: "userlanguages", localField: "_id", foreignField: "user_id", as: "userLanguage" } },
            { "$lookup": { from: "languages", localField: "userLanguage.language_id", foreignField: "_id", as: "userLanguage.languages" } },

            { "$lookup": { from: "userreviews", localField: "_id", foreignField: "user_id", as: "userReview" } },
            { $sort: { "userreviews.updatedAt": -1 } },

            { "$match": wrkObj },

            {
                "$project": {
                    "_id": 1, "bio": 1, "name": 1, "image": 1, "email": 1, "is_verified": 1, "business_years": 1, "agency": 1,"average":1,"is_deactivated":1,"deleted_at":1,
                    "insurances": "$userInsurance.insurances",
                    "licenses": "$userLicense.licenses",
                    "states": "$userState.states",
                    "languages": "$userLanguage.languages",
                    "review": "$userReview"

                }
            },
            { $sort: { "_id": -1 } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            }
            

        ]
        let user = await userSchema.aggregate(aggregationCriteria);
        let agent = []
        let getcount = 0;
        if (user[0].data.length > 0) {

            await Promise.all(user[0].data.map(async x => {

                let average = await ReviewModal.aggregate([
                    {
                        $match: { user_id: ObjectId(x._id), rating: { $ne: 0 } }
                    },
                    {
                        $group:
                        {
                            _id: "null",
                            averagetet: { $avg: "$rating" }
                        }
                    }
                ]);
                x.averageRating = 0
                if (average.length > 0) {
                    x.averageRating = average[0].averagetet
                }
                if (x.review.length > 0) {
                    x.review = x.review[0]
                }


            })
            )
            agent = user[0].data
            getcount = user[0].metadata[0].total
        }
        return ({ count: getcount, agent })
    }
    static async agentDetail(id){
        let user = await userSchema.aggregate([
            {
                $match: { _id: ObjectId(id) }
            },
            { "$lookup": { from: "userinsurances", localField: "_id", foreignField: "user_id", as: "userInsurance" } },
            { "$lookup": { from: "insurances", localField: "userInsurance.insurance_id", foreignField: "_id", as: "userInsurance.insurances" } },

            { "$lookup": { from: "userlanguages", localField: "_id", foreignField: "user_id", as: "userLanguage" } },
            { "$lookup": { from: "languages", localField: "userLanguage.language_id", foreignField: "_id", as: "userLanguage.languages" } },

            { "$lookup": { from: "userlicenses", localField: "_id", foreignField: "user_id", as: "userLicense" } },
            { "$lookup": { from: "licenses", localField: "userLicense.license_id", foreignField: "_id", as: "userLicense.licenses" } },

            { "$lookup": { from: "userstates", localField: "_id", foreignField: "user_id", as: "userState" } },
            { "$lookup": { from: "states", localField: "userState.state_id", foreignField: "_id", as: "userState.states" } },

            { "$lookup": { from: "usersubscriptions", localField: "_id", foreignField: "user_id", as: "userSubscription" } },

            { "$lookup": { from: "userreviews", localField: "_id", foreignField: "user_id", as: "userReview" } },

            { "$lookup": { from: "usercalendars", localField: "_id", foreignField: "user_id", as: "calendar" } },

            { "$lookup": { from: "blockdates", localField: "_id", foreignField: "user_id", as: "blockdates" } },
            
            {
                "$project": {
                    "_id": 1, "bio": 1, "name": 1, "image": 1, "phone": 1, "country_code": 1, "email": 1, "is_verified": 1, "email": 1, "agency": 1, "producer_number": 1, "business_years": 1, "subscription_status": 1, "subscription_id": 1, "card_attach": 1, "email_content": 1,"average":1,"is_deactivated":1,"deleted_at":1,
                    "insurances": "$userInsurance.insurances",
                    "licenses": "$userLicense.licenses",
                    "states": "$userState.states",
                    "languages": "$userLanguage.languages",
                    "subscription": "$userSubscription",
                    "language": "$language",
                    "calendar": "$calendar",
                    "block_dates": "$blockdates"

                }
            }

        ]);
        if (user[0].subscription_status != 0) {
            let getSubscriptionDetail = await SubscriptionModal.findOne({ _id: user[0].subscription[0].subscription_id })
            user[0].subscription = user[0].subscription[0]
            user[0].subscription.name = getSubscriptionDetail.name
        }
        else {
            user[0].subscription = {}
        }

        if (user[0].calendar.length > 0) {
            user[0].calendar = user[0].calendar[0]
            user[0].calendar.timeslot = await UserTimeslotModel.find({ calendar_id: user[0].calendar._id })
        }
        else {
            user[0].calendar = {}
        }
        user[0].review = await this.getReviews(id, 0, 10)

        return user[0]
    }
    static async actDeactUser(id,deactivate){
        let data = {}
        data.is_deactivated  = deactivate
        await userSchema.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let agent = await this.agentDetail(id)
        return({message:"status updated",agent})
    }
    static async agentVerified(id,verified){
        let data = {}
        data.is_verified  = verified
        await userSchema.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let agent = await this.agentDetail(id)
        return({message:"status updated",agent})
    }
    static async deleteAgent(id) {
        var current_date = new Date()
        let data = {}
        data.deleted_at  = current_date
        await userSchema.findOneAndUpdate({ "_id": id }, { $set: data }, { new: true, upset: true });
        let agent = await this.agentDetail(id)
        return({message:"Deleted",agent})
    }
    static async getReviews(user_id,page,limit,keyword) {
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        var match = {}
        let wrkObj = {}, arr = [];
        if (user_id) {
            arr.push({ "user_id": ObjectId(user_id) })
        }
        else {
            arr.push({ "_id": { "$exists": true } })
        }
        wrkObj = { $and: arr }
        let reviews = await ReviewModal.aggregate([
            { "$match": wrkObj },
            { "$lookup": { from: "reviewimages", localField: "_id", foreignField: "review_id", as: "reviewImages" } },
            { $sort: { "_id": -1 } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }]
                }
            }
            
        ]);
        let average = await ReviewModal.aggregate([
            { "$match": wrkObj },
            {
                $group:
                {
                    _id: "null",
                    averagetet: { $avg: "$rating" }
                }
            }
        ]);

        let review = {}

        if (reviews[0].data.length > 0) {
            let total = reviews[0].metadata[0].total
            review.reviews = reviews[0].data
            review.getcount = total
            review.average = average[0].averagetet

            let get5Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 5 }] });
            let get4Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 4 }] });
            let get3Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 3 }] });
            let get2Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 2 }] });
            let get1Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 1 }] });
            
            review.total5starRating = (parseInt(get5Star) * parseInt(100)) / parseInt(total)
            review.total4starRating = (parseInt(get4Star) * parseInt(100)) / parseInt(total)
            review.total3starRating = (parseInt(get3Star) * parseInt(100)) / parseInt(total)
            review.total2starRating = (parseInt(get2Star) * parseInt(100)) / parseInt(total)
            review.total1starRating = (parseInt(get1Star) * parseInt(100)) / parseInt(total)
        }
        return review
    }
    static async getSubscriptions(user_id,page,limit,keyword){

    }
    static async getBookings(user_id,page,limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let arr = []
        let wrkObj = {}
        if (user_id) {
            arr.push({ "user_id": ObjectId(user_id) })
        }
        else {
            arr.push({ "user_id": { "$exists": true } })
        }

        if(keyword){
            arr.push(
                {"$or":
                    [
                        {"first_name":{$regex:keyword}},
                        {"last_name":{$regex:keyword}},
                        {"email":{$regex:keyword}}
                        
                    ]
                }
            )
        }
        wrkObj = { $and: arr }
         
        let getBooking = await CustomerBookingModal.aggregate([
            {
                "$match": wrkObj 
            },
            { $sort: { "_id": -1 } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            }
            
        ]);
        let bookings = []
        let count = 0
        if (getBooking[0].data.length > 0) {
            bookings = getBooking[0].data
            count = getBooking[0].metadata[0].total
        }
        return ({ count: count, booking: bookings })
    }
    static async getNotifications(user_id,page,limit,keyword){
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        var match = {}
        let wrkObj = {}, arr = [];
        if (user_id) {
            arr.push({ "user_id": ObjectId(user_id) })
        }
        else {
            arr.push({ "_id": { "$exists": true } })
        }
        wrkObj = { $and: arr }
        let getNotifications = await NotificationSchema.aggregate([
            { "$match": wrkObj },
            { $sort: { _id: -1 } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }]
                }
            }
            
        ]);
        let notifications = []
        let count = 0
        if (getNotifications[0].data.length > 0) {
            notifications = getNotifications[0].data
            count = getNotifications[0].metadata[0].total
        }
        return ({ count: count, notifications })
    }

    


}
module.exports = AdminService;