const express = require('express');
const router = express.Router();
const UserController = require("./users.controller");
let auth = require("../../middleware/auth")


//user APIs
router.post('/signup', UserController.signup);
router.post('/login', UserController.login);
router.get('/static-data', UserController.staticData);
router.get('/resend/email', UserController.resendEmail);
router.get('/profile',auth.checkOptionalToken, UserController.getUserProfile);
router.put('/profile',auth.checkToken, UserController.updateUserProfile);
router.post('/change-password',auth.checkToken, UserController.changePassword);
router.post('/deactivate-account',auth.checkToken, UserController.deactivateAccount);
router.post('/forgot-password',UserController.forgotPassword);
router.post('/check-email-otp', UserController.checkEmailOtp);
router.post('/reset/password', auth.checkToken, UserController.resetPassword);

router.post('/add-calendar',auth.checkToken, UserController.addCalendar);
router.put('/calendar',auth.checkToken, UserController.editCalendar);
router.get('/calendar',auth.checkToken, UserController.getCalendar);
router.post('/review-request',auth.checkToken, UserController.reviewRequest);
router.post('/add-review',UserController.addReview);
router.get('/reviews',auth.checkOptionalToken,UserController.getReviews);
router.get('/agent-detail',auth.checkOptionalToken,UserController.getAgentDetail);
router.get('/bookings',auth.checkToken,UserController.getBookingsList);
router.post('/block-dates',auth.checkToken,UserController.blockDates);
router.post('/unblock-dates',auth.checkToken,UserController.unblockDates);
router.post('/unsubscribe',UserController.unsubscribe);
router.post('/cancel-appointment',auth.checkToken,UserController.cancelAppointment);
router.post('/invite-user',UserController.inviteUser);
router.get('/notifications',auth.checkToken,UserController.notifications);



//customer routes
router.get('/search-agent',UserController.searchAgent);
router.post('/add-booking',UserController.addBooking);
router.post('/schedule-call',UserController.scheduleCall);
router.post('/delete-card',auth.checkToken,UserController.deleteCard);
router.post('/notification-delete',auth.checkToken,UserController.notificationDelete);

//agent portal
router.get('/uwapp-agent',auth.checkToken,UserController.getUWappAgentPortal);
router.post('/uwapp-agent',auth.checkToken,UserController.addUWappAgentPortal);
router.post('/remove-uwapp-agent',auth.checkToken,UserController.removeUWappAgentPortal)


module.exports = router;