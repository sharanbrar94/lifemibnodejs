const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        default:""
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        default:""
    },
    country_code: {
        type: String,
        default:""
    },
    agency: {
        type: String,
        default:""
    },
    password: {
        type: String,
        required: true
    },
    is_deactivated: {
        type: Number,
        default:0,
        required: true
    },
    deleted_at: {
        type: Date,
        default:"",
    },
    deactivate_reason:{
        type: String,
        default:""
    },
    producer_number:{
        type: String,
        default:""
    },
    business_years:{
        type: Number,
        default:0
    },
    image:{
        type: String,
        default:""
    },
    customer_stripe_id:{
        type: String,
        default:""
    },
    subscription_id:{
        type: mongoose.Schema.ObjectId, 
        ref: 'subscriptions'
    },
    subscription_status:{
        type: Number,
        default:0
    },
    card_attach:{
        type: Number,
        default:0
    },
    is_verified:{
        type: Number,
        default:0
    },
    reset_otp:{
        type: Number,
        default:0
    },
    email_content:{
        type:String,
        default:"Thanks again for being a great client. I just signed up on Life Mib to find more amazing clients like you, and reviews are a large part of my profile. Can you take a minute to write a couple sentences about your experience working with me? I'd love for my future clients to hear from your experience first-hand."
    },
    bio : {
        type : String,
        default : null
    },
    average : {
        type : Number,
        default : 0
    }
    
//}, { timestamps: true });
//}, { timestamps: true, toObject: { virtuals: true, }, toJSON: { virtuals: true } });
// userSchema.virtual('subscription', {
//     ref: 'userSubscriptions',
//     localField: '_id',
//     foreignField: 'user_id',
//     justOne: false
// });
}, { timestamps: true, toObject: { virtuals: true, }, toJSON: { virtuals: true } });
userSchema.virtual('insurances', {
    ref: 'userInsurances',
    localField: '_id',
    foreignField: 'user_id',
    justOne: false
});
userSchema.virtual('licenses', {
    ref: 'userLicenses',
    localField: '_id',
    foreignField: 'user_id',
    justOne: false
});
userSchema.virtual('states', {
    ref: 'userStates',
    localField: '_id',
    foreignField: 'user_id',
    justOne: false
});
userSchema.virtual('rating', {
    ref: 'userReviews',
    localField: '_id',
    foreignField: 'user_id',
    justOne: false
});

module.exports = mongoose.model('users', userSchema);