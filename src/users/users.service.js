require("dotenv").config();
const { sign } = require("jsonwebtoken");
const userSchema = require('../users/model/users.model');
const LanguageModel = require('../../models/languages.model');
const LicenseModel = require('../../models/licenses.model');
const StateModel = require('../../models/states.model');
const InsuranceModel = require('../../models/insurances.model');
const UserInsuranceModel = require('../../models/user_insurances.model');
const UserLanguageModel = require('../../models/user_languages.model');
const UserLicenseModel = require('../../models/user_licenses.model');
const UserSpTermProviderModel = require('../../models/user_sp_term_providers.model');
const UserSpWholeProviderModel = require('../../models/user_sp_whole_providers.model');
const UserStateModel = require('../../models/user_states.model');
const UserCalendarModel = require('../../models/user_calendars.model');
const UserTimeslotModel = require('../../models/user_timeslots.model');
const SubscriptionModal = require('../../models/subscription.model');
const UserSubscriptionModal = require('../../models/user_subscription.model');
const ReviewModal = require('../../models/user_review.model');
const CustomerBookingModal = require('../../models/customer_bookings.model');
const ReviewImageModal = require('../../models/review_images.model');
const BlockDatesModal = require('../../models/block_dates.model');
const UnsubscribeModal = require('../../models/unsubscribes.model');
const NotificationSchema = require('../../models/notifications.model');
const UWappAgentPortalModel = require('../../models/uwapp_agent_portal.model');
const UwappMedicalHistoryModel = require('../../models/uwapp_medical_history.model');
const UwappPersonalHistoryModel = require('../../models/uwapp_personal_history.model')
let Common = require("../../middleware/common");
let cities = require('cities');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
if(process.env.STRIPE == "true"){
    var stripe = require('stripe')(process.env.STRIPE_LIVE_KEY);
}
else{
    var stripe = require('stripe')(process.env.STRIPE_TEST_KEY);
}

const { genSaltSync, hashSync, compareSync } = require("bcryptjs");

class UserService {
    static async signup(email, password) {
        let getUser = await userSchema.find({ email: email });
        if (getUser[0]) {
            throw "Email already exist!";
        }

        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        const newUser = new userSchema({ email, password });
        const data = await newUser.save();
        let userId = data._id
        await this.createCustomer(userId, email)

        let duration = 30
        let timezone = "Chicago-GMT-5"
        let timeslot = [
            {
                "day": 1,
                "start_time": "13:00",
                "end_time": "23:00"
            },
            {
                "day": 2,
                "start_time": "13:00",
                "end_time": "23:00"
            },
            {
                "day": 3,
                "start_time": "13:00",
                "end_time": "23:00"
            },
            {
                "day": 4,
                "start_time": "13:00",
                "end_time": "23:00"
            },
            {
                "day": 5,
                "start_time": "13:00",
                "end_time": "23:00"
            }

        ]
        await this.addCalendar(userId, timezone, duration, timeslot)
        let user = await this.getUserProfile(userId)
        const jsonData = await this.jsonDataFunc(user)
        let token = await this.generateToken(jsonData)

        //email//
        let subject = 'Welcome to The New Way Insurance Is Sold & Serviced'
        let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
        console.log(getSubscribe)
        let getUrl = process.env.URL
        if(getSubscribe == null){
            await Common.welcomeEmail(email, subject,getUrl);
        }
        return { token: token, ...user }
    }
    //Add user in stripe
    static async createCustomer(user_id, email) {
        console.log(user_id)
        const customer = await stripe.customers.create({
            email: email
        });
        let customer_id = customer['id'];
        let userData = {}
        userData.customer_stripe_id = customer_id
        return await userSchema.findOneAndUpdate({ "_id": user_id }, { $set: userData }, { new: true, upset: true });

    }
    static async generateToken(user) {
        let token = await sign({ user: user }, 'wLJhp7xEMd772mqUfxZhZaDN8Uy7JrQR', { expiresIn: "2 Days" });
        return token
    }

    static async getUserProfileByEmail(email) {
        return await userSchema.findOne({ email: email });
    }

    static async getUserProfileByPhone(phone, country_code) {
        return await userSchema.find({ phone: phone, country_code: country_code });
    }



    static async resendEmail(email, type, name) {
        // 1 for verficiation email, 2 for forget password
        if (type == 1) {
            let token = await this.generateTokenForUser("NULL", email);

            const subject = "Verify Email";

            const replacement = {
                name: name,
                email: email,
                link: `${process.env.WEBSITE_BASE_URL}/app/email-verify/${token}`,
            };

            const htmlBody = await Common.editEmailTemplate(
                "./config/emailVerificationTemplate.html",
                replacement
            );

            const result = await Common.sendMail(email, subject, htmlBody);

            return { token: token, message: "Mail sent to your email" };
        } else if (type == 2) await this.forgotPassword(email);
    }

    /*======Login=======*/
    static async login(inputs) {
        const { email, password } = inputs;
        let getUser = await this.getUser(email);

        if (getUser) {
            if (getUser.is_deactivated == 1) {
                throw ("Your account has been disabled, please contact the adminstrator.")
            }
            let passwordResult = compareSync(password, getUser.password);
            if (!passwordResult) {
                throw "Please enter correct password";
            }
        }
        else {
            throw "No account found with this email";
        }
        const jsonData = await this.jsonDataFunc(getUser)
        let auth_token = await this.generateToken(jsonData);
        let userId = getUser._id
        console.log("user", getUser, userId)
        let user = await this.getUserProfile(userId)
        return { message: "User login successfully", token: auth_token, ...user }
    }
    static async jsonDataFunc(user){
        let termProviders = await UserSpTermProviderModel.find({'user_id':user.id})
        let wholeProviders = await UserSpWholeProviderModel.find({'user_id':user.id})
        // const jsonData = user.toJSON();
        const jsonData = JSON.parse(JSON.stringify(user));
        jsonData.providers = []
        termProviders.forEach(async x=>{
            jsonData.providers.push(x.sp_provider_id)
        })
        wholeProviders.forEach(async x=>{
            jsonData.providers.push(x.sp_provider_id)
        })
        return jsonData
    }

    /*=====static data module====*/
    static async getLanguages() {
        const languages = await LanguageModel.find({ __v : 0 }).sort({ "name": 1 });
        return languages
    }
    static async getLicenses() {
        const licenses = await LicenseModel.find({ __v : 0 });
        return licenses
    }
    static async getStates() {
        const states = await StateModel.find({ __v : 0 });
        return states
    }
    static async getInsurances() {

        const insurances = await InsuranceModel.find({ __v : 0 });
        return insurances
    }
    static async getSpProviders(){
        const spProviders = await UserSpProviderModel.find({deleted_at: null});
    }
    static async getSubscription() {
        const subscription = await SubscriptionModal.find();
        return subscription
    }

    static async getUser(email) {
        return await userSchema.findOne({ email: email });
    }
    static async getUserProfile(id) {
        let user = await userSchema.aggregate([
            {
                $match: { _id: ObjectId(id) }
            },
            { "$lookup": { from: "userinsurances", localField: "_id", foreignField: "user_id", as: "userInsurance" } },
            { "$lookup": { from: "insurances", localField: "userInsurance.insurance_id", foreignField: "_id", as: "userInsurance.insurances" } },

            { "$lookup": { from: "userlanguages", localField: "_id", foreignField: "user_id", as: "userLanguage" } },
            { "$lookup": { from: "languages", localField: "userLanguage.language_id", foreignField: "_id", as: "userLanguage.languages" } },

            { "$lookup": { from: "userlicenses", localField: "_id", foreignField: "user_id", as: "userLicense" } },
            { "$lookup": { from: "licenses", localField: "userLicense.license_id", foreignField: "_id", as: "userLicense.licenses" } },

            { "$lookup": { from: "usersptermproviders", localField: "_id", foreignField: "user_id", as: "userSpTermProvider" } },
            { "$lookup": { from: "userspwholeproviders", localField: "_id", foreignField: "user_id", as: "userSpWholeProvider" } },

            { "$lookup": { from: "userstates", localField: "_id", foreignField: "user_id", as: "userState" } },
            { "$lookup": { from: "states", localField: "userState.state_id", foreignField: "_id", as: "userState.states" } },

            { "$lookup": { from: "usersubscriptions", localField: "_id", foreignField: "user_id", as: "userSubscription" } },

            { "$lookup": { from: "userreviews", localField: "_id", foreignField: "user_id", as: "userReview" } },

            { "$lookup": { from: "usercalendars", localField: "_id", foreignField: "user_id", as: "calendar" } },

            { "$lookup": { from: "blockdates", localField: "_id", foreignField: "user_id", as: "blockdates" } },

            {
                "$project": {
                    "_id": 1, "bio": 1, "name": 1, "image": 1, "phone": 1, "country_code": 1, "email": 1, "is_verified": 1, "email": 1, "agency": 1, "producer_number": 1, "business_years": 1, "subscription_status": 1, "subscription_id": 1, "card_attach": 1, "email_content": 1,"average":1,
                    "insurances": "$userInsurance.insurances",
                    "licenses": "$userLicense.licenses",
                    "states": "$userState.states",
                    "languages": "$userLanguage.languages",
                    "subscription": "$userSubscription",
                    "sp_term_providers":"$userSpTermProvider",
                    "sp_whole_providers":"$userSpWholeProvider",
                    "language": "$language",
                    "calendar": "$calendar",
                    "block_dates": "$blockdates"

                }
            }

        ]);

        if (user[0].subscription_status != 0) {
            let getSubscriptionDetail = await SubscriptionModal.findOne({ _id: user[0].subscription[0].subscription_id })
            user[0].subscription = user[0].subscription[0]
            user[0].subscription.name = getSubscriptionDetail.name
        }
        else {
            user[0].subscription = {}
        }

        if (user[0].calendar.length > 0) {
            user[0].calendar = user[0].calendar[0]
            user[0].calendar.timeslot = await UserTimeslotModel.find({ calendar_id: user[0].calendar._id })
        }
        else {
            user[0].calendar = {}
        }
        user[0].review = await this.getReviews(id, 0, 10)

        return user[0]

    }
    static async getReview(user_id, page, limit) {
        let reviews = await this.getReviews(user_id, page, limit)
        return reviews
    }
    static async getReviews(user_id, page, limit) {
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let reviews = await ReviewModal.aggregate([
            {
                $match: { user_id: ObjectId(user_id), rating: { $ne: 0 } }
            },
            { "$lookup": { from: "reviewimages", localField: "_id", foreignField: "review_id", as: "reviewImages" } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }]
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let average = await ReviewModal.aggregate([
            {
                $match: { user_id: ObjectId(user_id), rating: { $ne: 0 } }
            },
            {
                $group:
                {
                    _id: "null",
                    averagetet: { $avg: "$rating" }
                }
            }
        ]);

        let review = {}

        if (reviews[0].data.length > 0) {
            let total = reviews[0].metadata[0].total
            review.reviews = reviews[0].data
            review.getcount = total
            review.average = average[0].averagetet



            let get5Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 5 }] });
            let get4Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 4 }] });
            let get3Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 3 }] });
            let get2Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 2 }] });
            let get1Star = await ReviewModal.countDocuments({ $and: [{ "user_id": user_id }, { "rating": 1 }] });
            console.log(get4Star)

            review.total5starRating = (parseInt(get5Star) * parseInt(100)) / parseInt(total)
            review.total4starRating = (parseInt(get4Star) * parseInt(100)) / parseInt(total)
            review.total3starRating = (parseInt(get3Star) * parseInt(100)) / parseInt(total)
            review.total2starRating = (parseInt(get2Star) * parseInt(100)) / parseInt(total)
            review.total1starRating = (parseInt(get1Star) * parseInt(100)) / parseInt(total)
        }
        return review
    }
    static async getAgentDetail(token) {
        let reviewDetail = await ReviewModal.findOne({ "token": token })
        let user_id = reviewDetail.user_id
        return await this.getUserProfile(user_id)
    }

    static async editProviders(user_id, licenses) {
        await UserLicenseModel.deleteMany({ user_id: user_id });
        return licenses.forEach(async x => {
            const data = new UserLicenseModel({ "user_id": user_id, "license_id": x });
            data.save();
        })
    }

    static async editSpTermProviders(user_id, sp_term_providers) {
        await UserSpTermProviderModel.deleteMany({ user_id: user_id });
        return sp_term_providers.forEach(async x => {
            const data = new UserSpTermProviderModel({ "user_id": user_id, "sp_provider_id": x.sp_provider_id, 'sp_provider_title': x.sp_provider_title });
            data.save();
        })
    }

    static async editSpWholeProviders(user_id, sp_whole_providers) {
        await UserSpWholeProviderModel.deleteMany({ user_id: user_id });
        return sp_whole_providers.forEach(async x => {
            const data = new UserSpWholeProviderModel({ "user_id": user_id, "sp_provider_id": x.sp_provider_id, 'sp_provider_title': x.sp_provider_title });
            data.save();
        })
    }
    
    static async editStates(user_id, states) {
        await UserStateModel.deleteMany({ user_id: user_id });
        return states.forEach(async x => {
            const data = new UserStateModel({ "user_id": user_id, "state_id": x });
            data.save();
        })

    }
    static async editInsurances(user_id, insurances) {
        await UserInsuranceModel.deleteMany({ user_id: user_id });
        return insurances.forEach(async x => {
            const data = new UserInsuranceModel({ "user_id": user_id, "insurance_id": x });
            data.save();
        })
    }
    static async editLanguage(user_id, languages) {
        await UserLanguageModel.deleteMany({ user_id: user_id });
        return languages.forEach(async x => {
            const data = new UserLanguageModel({ "user_id": user_id, "language_id": x });
            data.save();
        })
    }

    static async updateUserProfile(user_id, name, phone, agency, country_code, image, language, producer_number, business_years, insurances, licenses, sp_term_providers, sp_whole_providers, states, subscription_id, stripe_token, subscription_email, subscription_name, email_content, bio) {
        let userData = {}

        if (insurances != undefined) {
            await this.editInsurances(user_id, insurances)
        }
        if (licenses != undefined) {
            await this.editProviders(user_id, licenses)
        }
        if(sp_term_providers != undefined){
            await this.editSpTermProviders(user_id, sp_term_providers)
        }
        if(sp_whole_providers != undefined){
            await this.editSpWholeProviders(user_id, sp_whole_providers)
        }
        if (states != undefined) {
            await this.editStates(user_id, states)
        }
        if (language != undefined) {
            await this.editLanguage(user_id, language)
        }

        if (name != undefined) {
            userData.name = name;
        }
        if (email_content != undefined) {
            userData.email_content = email_content;
        }
        if (image != undefined) {
            userData.image = image;
        }
        if (phone != undefined) {
            userData.phone = phone;
        }
        if (agency != undefined) {
            userData.agency = agency;
        }
        if (country_code != undefined) {
            userData.country_code = country_code;
        }
        if (language != undefined) {
            userData.language = language;
        }
        if (producer_number != undefined) {
            userData.producer_number = producer_number;
        }
        if (business_years != undefined) {
            userData.business_years = business_years;
        }
        if (bio != undefined) {
            userData.bio = bio;
        }
        if (subscription_id != undefined) {
            await this.createSubscription(subscription_id, user_id, stripe_token, subscription_email, subscription_name)
        }
        await userSchema.findOneAndUpdate({ "_id": user_id }, { $set: userData }, { new: true, upset: true });



        let user = await this.getUserProfile(user_id)
        return { message: "Profile updated", ...user }
    }
    static async addNotification(title,notification,type,user_id){
        const addNotif = new NotificationSchema({ "title":title,"notification": notification,"type":type,"user_id":user_id });
        return await addNotif.save();
    }
    /*======subscription module=====*/
    static async createSubscription(subscription_id, user_id, stripe_token, subscription_email, subscription_name) {
        let userData = {}
        const subscriptionDetail = await SubscriptionModal.findOne({ "_id": subscription_id });
        const userSubscriptionDetail = await UserSubscriptionModal.findOne({ "user_id": user_id });
        const userDetail = await userSchema.findOne({ "_id": user_id });

        let date = new Date();
        let addMonth = date.getMonth() + 1;
        let addOneMonth = date.getMonth() + 2;
        let getStartDate = date.getFullYear() + '-' + addMonth + '-' + date.getDate()
        let getEndDate = date.getFullYear() + '-' + addOneMonth + '-' + date.getDate()
        
        if (subscriptionDetail.subscription_id == '') {
            //free plan
            //userData.subscription_id = subscription_id
            userData.subscription_status = 1
            let subsData = {}
            
            if (userSubscriptionDetail != null) {
                subsData.cancel_at = userSubscriptionDetail.enddate
                subsData.upgrade_at = userSubscriptionDetail.enddate
                subsData.next_subscription_id = subscription_id
                subsData.next_subscription_name = subscriptionDetail.name
                subsData.next_subscription_price = subscriptionDetail.price
                //cancel subscription at billing end
                // stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {cancel_at_period_end: true});
                await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });
            }
            else {
                //add free plan
                userData.card_attach = 0
                const addSubscription = new UserSubscriptionModal({ "user_id": user_id, "subscription_id": subscription_id });
                await addSubscription.save();
            }
            
        }
        else {
           
            let customer_id = userDetail.customer_stripe_id
            let plan_id = subscriptionDetail.subscription_id

            if (userDetail.card_attach == 0) {
                //attach card to user
                const paymentMethod = await stripe.paymentMethods.create({
                    type: 'card',
                    card: {
                        number: '4242424242424242',
                        exp_month: 9,
                        exp_year: 2022,
                        cvc: '314',
                    },
                });
                let tokenId = paymentMethod.id
                //let tokenId = stripe_token

                //Attach card to customer
                const attachPaymentToCustomer = await stripe.paymentMethods.attach(
                    tokenId,
                    { customer: customer_id }
                );

                //Set default source to customer
                const attchSourceToCustomer = await stripe.customers.update(
                    customer_id,
                    { invoice_settings: { default_payment_method: tokenId } }
                );
                let subData = {}

                subData.card_number = attachPaymentToCustomer.card.last4
                subData.exp_month = attachPaymentToCustomer.card.exp_month
                subData.exp_year = attachPaymentToCustomer.card.exp_year
                subData.brand = attachPaymentToCustomer.card.brand
                subData.startdate = getStartDate
                subData.enddate = getEndDate
                subData.subscription_email = subscription_email
                subData.subscription_name = subscription_name
                if (userSubscriptionDetail != null) {
                    await this.subscriptionUpdate(userSubscriptionDetail, subscriptionDetail,userDetail,subscription_id,user_id)
                    await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subData }, { new: true, upset: true });
                    
                }
                else {
                    //Buy sybscription
                    const newSubscription = await stripe.subscriptions.create({
                        customer: customer_id,
                        items: [{ price: plan_id }]
                    });
                    const getSubId = newSubscription.id
                    
                    const addSubscription = new UserSubscriptionModal({ "user_id": user_id,"card_number":attachPaymentToCustomer.card.last4,"exp_month":attachPaymentToCustomer.card.exp_month, "exp_year":attachPaymentToCustomer.card.exp_year,"brand":attachPaymentToCustomer.card.brand,"startdate":getStartDate,"enddate":getEndDate,"subscription_email":subscription_email,"subscription_name":subscription_name,"subscription_id": subscription_id, "price": subscriptionDetail.price, subscribe_id: getSubId });
                    await addSubscription.save();
                }
                 
            }
            else {
                //change plan
                if(userSubscriptionDetail.subscription_status == 0){
                    await this.createNewSubscription(customer_id,plan_id,subscriptionDetail,getStartDate,getEndDate,subscription_id,user_id)
                }
                else{
                    await this.subscriptionUpdate(userSubscriptionDetail, subscriptionDetail,userDetail,subscription_id,user_id)
                }
            }

            userData.subscription_id = subscription_id
            userData.subscription_status = 2
            userData.card_attach = 1

            //email//
            let subject = 'Welcome to The New Way Insurance Is Sold & Serviced'
            let getSubscribe = await UnsubscribeModal.findOne({ "email": userDetail.email });
            if(getSubscribe == null){
                let getUrl = process.env.URL
                await Common.subscription(userDetail.email, subject, userDetail.name,getUrl);
            }
            
        }
        let title = "Subscription"
        let notification = "You subscribed for "+subscriptionDetail.name
        let type = "subscription"
        await this.addNotification(title,notification,type,user_id)

        return await userSchema.findOneAndUpdate({ "_id": user_id }, { $set: userData }, { new: true, upset: true });
    }

    /*======subscription module=====*/
    // static async createSubscription(subscription_id, user_id, stripe_token, subscription_email, subscription_name) {
    //     let userData = {}
    //     const subscriptionDetail = await SubscriptionModal.findOne({ "_id": subscription_id });
    //     const userSubscriptionDetail = await UserSubscriptionModal.findOne({ "user_id": user_id });
    //     const userDetail = await userSchema.findOne({ "_id": user_id });

    //     let date = new Date();
    //     let addOneMonth = date.getMonth() + 1;
    //     let getStartDate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate()
    //     let getEndDate = date.getFullYear() + '-' + addOneMonth + '-' + date.getDate()
        
    //     if (subscriptionDetail.subscription_id == '') {
    //         userData.subscription_id = subscription_id
    //         userData.subscription_status = 1
    //         let subsData = {}
    //         subsData.price = 0
    //         subsData.subscription_id = subscription_id
    //         subsData.subscription_status = 0

    //         if (userSubscriptionDetail != null) {
    //             const deleted = await stripe.subscriptions.del(
    //                 userSubscriptionDetail.subscribe_id
    //             );
    //             await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });

    //         }
    //         else {
               

    //             userData.card_attach = 0
    //             const addSubscription = new UserSubscriptionModal({ "user_id": user_id, "subscription_id": subscription_id });
    //             await addSubscription.save();
    //         }
            
    //     }
    //     else {
           
    //         let customer_id = userDetail.customer_stripe_id
    //         let plan_id = subscriptionDetail.subscription_id

    //         if (userDetail.card_attach == 0) {
    //             const paymentMethod = await stripe.paymentMethods.create({
    //                 type: 'card',
    //                 card: {
    //                     number: '4242424242424242',
    //                     exp_month: 9,
    //                     exp_year: 2022,
    //                     cvc: '314',
    //                 },
    //             });
    //             let tokenId = paymentMethod.id
    //             //let tokenId = stripe_token

    //             //Attach card to customer
    //             const attachPaymentToCustomer = await stripe.paymentMethods.attach(
    //                 tokenId,
    //                 { customer: customer_id }
    //             );

    //             //Set default source to customer
    //             const attchSourceToCustomer = await stripe.customers.update(
    //                 customer_id,
    //                 { invoice_settings: { default_payment_method: tokenId } }
    //             );

    //             let card_number = attachPaymentToCustomer.card.last4
    //             let exp_month = attachPaymentToCustomer.card.exp_month
    //             let exp_year = attachPaymentToCustomer.card.exp_year
    //             let brand = attachPaymentToCustomer.card.brand
    //             if (userSubscriptionDetail != null) {
    //                 const newSubscription = await stripe.subscriptions.create({
    //                     customer: customer_id,
    //                     items: [{ price: plan_id }]
    //                 });
    //                 const getSubId = newSubscription.id

    //                 let subsData = {}
    //                 subsData.price = subscriptionDetail.price
    //                 subsData.startdate = getStartDate
    //                 subsData.enddate = getEndDate
    //                 subsData.subscription_id = subscription_id
    //                 subsData.subscribe_id = getSubId
    //                 subsData.card_number = card_number
    //                 subsData.brand = brand
    //                 subsData.exp_month = exp_month
    //                 subsData.exp_year = exp_year
    //                 subsData.subscription_email = subscription_email
    //                 subsData.subscription_name = subscription_name
    //                 await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });
    //             }
    //             else {
    //                 //Buy sybscription
    //                 const newSubscription = await stripe.subscriptions.create({
    //                     customer: customer_id,
    //                     items: [{ price: plan_id }]
    //                 });
    //                 const getSubId = newSubscription.id
    //                 console.log(newSubscription)

    //                 const addSubscription = new UserSubscriptionModal({ "user_id": user_id, "subscription_id": subscription_id, "price": subscriptionDetail.price, card_number: card_number, startdate: getStartDate, enddate: getEndDate, exp_month: exp_month, exp_year: exp_year, brand: brand, subscription_email: subscription_email, subscription_name: subscription_name, subscribe_id: getSubId });
    //                 await addSubscription.save();


    //             }

    //         }
    //         else {
    //             console.log(userSubscriptionDetail)
    //             let subsData = {}
    //             subsData.price = subscriptionDetail.price
    //             subsData.startdate = getStartDate
    //             subsData.enddate = getEndDate
    //             subsData.subscription_id = subscription_id

    //             if(userSubscriptionDetail.subscription_status == 0){
    //                 const newSubscription = await stripe.subscriptions.create({
    //                     customer: customer_id,
    //                     items: [{ price: plan_id }]
    //                 });
    //                 var update_subscription_id = newSubscription.id
    //             }
    //             else{
    //                 let changeSubscription = await stripe.subscriptions.retrieve(userSubscriptionDetail.subscribe_id);
                
    //                 let updateSubscription = await stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {
    //                     billing_cycle_anchor: 'now',
    //                     proration_behavior: 'create_prorations',
    //                     items: [
    //                         {
    //                             id: changeSubscription.items.data[0].id,
    //                             price: subscriptionDetail.subscription_id,
    //                         },
    //                     ],
    //                 });

    //                 var update_subscription_id = updateSubscription.id
    //             }
    //             subsData.subscribe_id = update_subscription_id

    //             await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });
    //         }

    //         userData.subscription_id = subscription_id
    //         userData.subscription_status = 2
    //         userData.card_attach = 1

    //         //email//
    //         let subject = 'Welcome to The New Way Insurance Is Sold & Serviced'
    //         let getSubscribe = await UnsubscribeModal.findOne({ "email": userDetail.email });
    //         if(getSubscribe == null){
    //             let getUrl = process.env.URL
    //             //await Common.subscription(userDetail.email, subject, userDetail.name,getUrl);
    //         }
            
    //     }
    //     let title = "Subscription"
    //     let notification = "You subscribed for "+subscriptionDetail.name
    //     let type = "subscription"
    //     await this.addNotification(title,notification,type,user_id)

    //     return await userSchema.findOneAndUpdate({ "_id": user_id }, { $set: userData }, { new: true, upset: true });
    // }
    static async createNewSubscription(customer_id,plan_id,subscriptionDetail,getStartDate,getEndDate,subscription_id,user_id){
        let subsData = {}
        const newSubscription = await stripe.subscriptions.create({
            customer: customer_id,
            items: [{ price: plan_id }]
        });
        var update_subscription_id = newSubscription.id
        subsData.price = subscriptionDetail.price
        subsData.startdate = getStartDate
        subsData.enddate = getEndDate
        subsData.subscription_id = subscription_id
        subsData.subscribe_id = update_subscription_id
        return await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true })
    }
    static async subscriptionUpdate(userSubscriptionDetail, subscriptionDetail,userDetail,subscription_id,user_id) {
        let subsData = {}
        if(subscriptionDetail.price > userSubscriptionDetail.price){
            if(userDetail.subscription_status == 1){
                var customer_id = userDetail.customer_stripe_id
                var plan_id = subscriptionDetail.subscription_id
                var updateSubscription = await stripe.subscriptions.create({
                    customer: customer_id,
                    items: [{ price: plan_id }]
                });
    
            }
            else{
                var changeSubscription = await stripe.subscriptions.retrieve(userSubscriptionDetail.subscribe_id);
                var updateSubscription = await stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {
                    cancel_at_period_end: false,
                    proration_behavior: 'create_prorations',
                    items: [
                        {
                            id: changeSubscription.items.data[0].id,
                            price: subscriptionDetail.subscription_id,
                        },
                    ],
                });
            }
            subsData.price = subscriptionDetail.price    
            //subsData.subscription_name = subscriptionDetail.name
            subsData.subscription_id = subscription_id
            subsData.subscribe_id = updateSubscription.id  
            subsData.subscription_status = 1
            subsData.upgrade_at = ''
            subsData.cancel_at = '' 
            subsData.next_subscription_id = ''
            subsData.next_subscription_name = ''
            subsData.next_subscription_price = ''
        }
        else{
            var changeSubscription = await stripe.subscriptions.retrieve(userSubscriptionDetail.subscribe_id);
            var updateSubscription = await stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {
                cancel_at_period_end : false,
                items: [
                    {
                        id: changeSubscription.items.data[0].id,
                        price: subscriptionDetail.subscription_id,
                    },
                ],
                prorate : false,
                trial_end : changeSubscription.current_period_end
            });
            subsData.upgrade_at = userSubscriptionDetail.enddate    
            subsData.cancel_at = '' 
            subsData.next_subscription_id = subscription_id
            subsData.next_subscription_name = subscriptionDetail.name
            subsData.next_subscription_price = subscriptionDetail.price
            
        }
        return await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });
    }
    static async subscriptionUpdate(userSubscriptionDetail, subscriptionDetail,userDetail,subscription_id,user_id) {
        let subsData = {}
        if(subscriptionDetail.price > userSubscriptionDetail.price){
            if(userDetail.subscription_status == 1){
                var customer_id = userDetail.customer_stripe_id
                var plan_id = subscriptionDetail.subscription_id
                var updateSubscription = await stripe.subscriptions.create({
                    customer: customer_id,
                    items: [{ price: plan_id }]
                });
    
            }
            else{
                var changeSubscription = await stripe.subscriptions.retrieve(userSubscriptionDetail.subscribe_id);
                var updateSubscription = await stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {
                    cancel_at_period_end: false,
                    proration_behavior: 'create_prorations',
                    items: [
                        {
                            id: changeSubscription.items.data[0].id,
                            price: subscriptionDetail.subscription_id,
                        },
                    ],
                });
            }
            subsData.price = subscriptionDetail.price    
            //subsData.subscription_name = subscriptionDetail.name
            subsData.subscription_id = subscription_id
            subsData.subscribe_id = updateSubscription.id  
            subsData.subscription_status = 1
            subsData.upgrade_at = ''
            subsData.cancel_at = '' 
            subsData.next_subscription_id = ''
            subsData.next_subscription_name = ''
            subsData.next_subscription_price = ''
        }
        else{
            var changeSubscription = await stripe.subscriptions.retrieve(userSubscriptionDetail.subscribe_id);
            var updateSubscription = await stripe.subscriptions.update(userSubscriptionDetail.subscribe_id, {
                cancel_at_period_end : false,
                items: [
                    {
                        id: changeSubscription.items.data[0].id,
                        price: subscriptionDetail.subscription_id,
                    },
                ],
                prorate : false,
                trial_end : changeSubscription.current_period_end
            });
            subsData.upgrade_at = userSubscriptionDetail.enddate    
            subsData.cancel_at = '' 
            subsData.next_subscription_id = subscription_id
            subsData.next_subscription_name = subscriptionDetail.name
            subsData.next_subscription_price = subscriptionDetail.price
            
        }
        return await UserSubscriptionModal.findOneAndUpdate({ "user_id": user_id }, { $set: subsData }, { new: true, upset: true });
    }



    /*======change password=======*/
    static async changePassword(user_id, new_password, old_password) {
        const userID = await userSchema.findById(user_id);
        let result = compareSync(old_password, userID.password);
        console.log(result)
        if (!result) {
            throw ('Please enter correct old password!');
        }
        else {
            const salt = genSaltSync(10);
            let password = hashSync(new_password, salt);
            userID.password = password
            userID.save();
            return { message: "Password changed" }
        }
    }

    /*=======deactivate account========*/
    static async deactivateAccount(user_id, deactivate_reason) {
        const userID = await userSchema.findById(user_id);
        userID.is_deactivated = 1
        userID.deactivate_reason = deactivate_reason
        userID.save();
        return { message: "Account deactivated" }
    }

    /*======forgot password module=====*/

    static async forgotPassword(email) {
        let user = await this.getUserProfileByEmail(email);

        if (user != null) {
            let otp = Math.floor(1000 + Math.random() * 9000);
            let pass = await this.updateResetPasswordToken(email, otp)
            let subject = 'Reset your password | Life Mib'
            let text = 'Your reset password OTP'
            let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
            if(getSubscribe == null){
                let getUrl = process.env.URL
                await Common.sendMail(email, subject, text, otp,getUrl);
                return { message: "Email Sent for reset password, please check."};
            }
            else{
                throw "You are unsubscribed unable to send email.";
            }
            
            
        } else {
            throw "No account found with this email.";
        }
    }
    static async checkEmailOtp(otp) {

        const userData = await userSchema.findOne({ $and: [{ reset_otp: otp }, { reset_otp: { $ne: 0 } }] });
        if (userData != null) {
            const jsonData = await this.jsonDataFunc(userData);
            let token = await this.generateToken(jsonData);
            let userId = userData._id

            let data = {}
            data.reset_otp = 0
            await userSchema.findOneAndUpdate({ "_id": userId }, { $set: data }, { new: true, upset: true });

            let user = await this.getUserProfile(userId)

            return { token: token, ...user }
        }
        else {
            throw ("Wrong otp");
        }
    }
    static async resetPassword(user_id, password) {
        console.log("user_id", user_id)
        const userData = await userSchema.findOne({ "_id": user_id });
        console.log("data", userData)
        if (userData != '') {
            await this.resetpass(userData.email, password)
            return ({ message: "Password changed!" })
        }
        else {
            throw ('Invalid user');
        }
    }
    static async resetpass(email, password) {
        console.log("email", email)
        const salt = genSaltSync(10);
        password = hashSync(password, salt);
        let data = {}
        data.password = password
        //data.reset_otp = 0
        return await userSchema.findOneAndUpdate({ "email": email }, { $set: data }, { new: true, upset: true });
    }


    static async generateTokenForUser(id, email) {
        let token = sign({ id, email }, "secret", {
            expiresIn: "1h",
        });
        return token;
    }

    static async updateResetPasswordToken(email, otp) {
        const userID = await userSchema.findOne({ "email": email });
        console.log("das", userID)
        userID.reset_otp = otp
        return await userID.save();


    }

    static async logout(req, res, next) {
        try {
            let fcm_id = req.body.fcm_id;
            console.log("fcm_id", fcm_id);
            await UserService.logout(fcm_id);
            return res
                .status(200)
                .json({ message: "You are logged out successfully!" });
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({ error: "bad_request", error_description: err });
        }
    }


    static async addCalendar(user_id, timezone, duration, timeslot) {
        const calendar = new UserCalendarModel({ user_id, timezone, duration });
        let data = await calendar.save();
        let calendarId = data._id
        timeslot.forEach(async x => {

            let day = x.day
            let startTime = x.start_time
            let endTime = x.end_time
            let time = new UserTimeslotModel({ calendar_id: calendarId, day: day, start_time: startTime, end_time: endTime });
            time.save();
        })
        let response = await this.getCalendarDetail(calendarId)
        return { message: "Calendar added", response };
    }
    static async editCalendar(calendar_id, timezone, duration, timeslot) {
        let data = {}
        data.timezone = timezone
        data.duration = duration
        await UserCalendarModel.findOneAndUpdate({ "_id": calendar_id }, { $set: data }, { new: true, upset: true });

        await UserTimeslotModel.deleteMany({ "calendar_id": calendar_id });
        timeslot.forEach(async x => {
            let day = x.day
            let startTime = x.start_time
            let endTime = x.end_time
            let time = new UserTimeslotModel({ calendar_id: calendar_id, day: day, start_time: startTime, end_time: endTime });
            time.save();
        })
        let response = await this.getCalendarDetail(calendar_id)
        return { message: "Calendar updated", response };
    }
    static async getCalendar(user_id) {
        let calendar = await UserCalendarModel.findOne({ "user_id": user_id });
        let calendarId = calendar._id
        let calendarTimeSlot = await UserTimeslotModel.find({ "calendar_id": calendarId });
        return { calendar, calendarTimeSlot }
    }
    static async getCalendarDetail(calendarId) {
        const calendar = await UserCalendarModel.findOne({ "_id": calendarId });
        let calendarTimeSlot = await UserTimeslotModel.find({ "calendar_id": calendarId });
        return { calendar, calendarTimeSlot }
    }
    static async reviewRequest(user_id, email) {
        let flag1 = 0
        let flag2 = 0
        await Promise.all(email.map(async x => {
            let token = Date.now()
            const getEmail = await ReviewModal.findOne({ "email": x });
            let getSubscribe = await UnsubscribeModal.findOne({ "email": x });
            
            if (getEmail == null && getSubscribe == null) {
                flag1 = 1
                const review = new ReviewModal({ "user_id": user_id, "email": x, "token": token,"invite_token":token });

                let userData = await userSchema.findOne({ "_id": user_id });
                if (userData.image != "") {
                    var image = userData.image
                }
                else {
                    var image = "1633080085800-user.png"
                }
                let name = userData.name
                let content = userData.email_content


                //email//
                let subject = 'Thanks again for being a great client'
                let link = process.env.URL+"review/" + token
                let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
                console.log("email",email)
                if(getSubscribe == null){
                    let getUrl = process.env.URL
                    await Common.reviewEmail(email, subject, link, image, name, content,getUrl);
                }
                await review.save();
                const index = email.indexOf(x);
                if (index > -1) {
                    email.splice(index, 1);
                }
            }
            else{
                flag2 = 1
            }

        }))
        let message = "Error while sending email..."
        if(flag1 == 1 && flag2 == 0){
            message = "Review request send successfully"
        }
        else if (flag1 == 0 && flag2 == 1){
            message = "Oops, can't send request, email address already used for review."
        }
        else if (flag1 == 1 && flag2 == 1){
            message = "Some Review request send and some review request not send " 
        }
        return ({ message: message, emails: email })
    }

    static async checkReviewEmail(email) {

        let reviews = await ReviewModal.find({ "email": { $in: email } });

        console.log("reviewww", reviews);

        if (!reviews.length) {
            return {
                emails: email
            }
        }
        else {
            let emails = [];

            reviews.forEach((review, index) => {

                let emailIndex = emails.indexOf(review.email);

                if (emailIndex == -1) {
                    emails.push(review.email);

                }


            })

            return {
                emails: emails
            }
        }

        if (review != null) {
            return {
                'exists': true
            }
        }
        else {
            return {
                'exists': false
            }
        }

    }

    static async addReview(token, rating, review, name, icon, images) {
        let getUrl = process.env.URL
        const getReview = await ReviewModal.findOne({ "token": token });
        let userDetail = await userSchema.findOne({ "_id": getReview.user_id });
        if (getReview != null) {
            let user_id = getReview.user_id
            let data = {}
            data.rating = rating
            data.review = review
            data.name = name
            data.icon = icon
            data.token = ""
            await ReviewModal.findOneAndUpdate({ "token": token }, { $set: data }, { new: true, upset: true });
            
            let getUserReviewCount = await ReviewModal.countDocuments({"user_id":user_id,"rating": { $ne: 0 }})

            let getUserReviewSum = await ReviewModal.aggregate([ 
                {
                    $match: { user_id: ObjectId(user_id), rating: { $ne: 0 } }
                },
                { 
                    
                    $group: { 
                        _id: "", 
                        sum: {  $sum: "$rating" }
                    } 
                } 
            ])
            let getTotal = parseFloat(getUserReviewSum[0].sum)/parseFloat(getUserReviewCount)
            let averageRating = getTotal.toFixed(1)

            let getData = {}
            getData.average = averageRating
            await userSchema.findOneAndUpdate({ "_id": user_id }, { $set: getData }, { new: true, upset: true });
            
            let arr = []
            images.forEach(async x => {

                let review_id = getReview._id
                const reviewData = new ReviewImageModal({ "review_id": review_id, "image": x });
                reviewData.save();


            })
            //email//
            let subject = 'New review'
            let getSubscribe = await UnsubscribeModal.findOne({ "email": userDetail.email });
            if(getSubscribe == null){
                
                await Common.newReview(userDetail.email, subject, name, review,getUrl);
            }
            
            //email to user
            let getSubject = 'Thank you!'
            let getSubscribes = await UnsubscribeModal.findOne({ "email": getReview.email });
            let getToken = getReview.invite_token
            let agentName = userDetail.name
            if(getSubscribes == null){
                await Common.reviewUserEmail(getReview.email, getSubject,getToken,getUrl,agentName);
            }
            return { message: "Review send successfully" }
        }
        else {
            throw ("Invalid email token")
        }
    }

    static async searchAgent(page, limit, pincode, keyword, license_id, insurance_id, state_id, language_id, review) {
        console.log("review",review)
        var match = {}
        let license = {}
        let insurance = {}
        let wrkObj = {}, arr = [];

        arr.push({ "is_deactivated": 0 })
        if (pincode) {

            let stateResponse = cities.zip_lookup(pincode);


            if (stateResponse != null) {

                console.log("state response ", stateResponse);


                let states = await StateModel.findOne({ "name": stateResponse.state.toUpperCase() });

                if (states != null) {
                    state_id = states._id;

                    console.log("state id ", state_id);
                }
                else {
                    return ({ count: 0, agent: [] })

                }

            }
            else {
                return ({ count: 0, agent: [] })
            }

        }
        if(review){
            if(review == 1){
                arr.push({average:{$gte:1,$lt:2}})
            }
            else if(review == 2){
                arr.push({average:{$gte:2,$lt:3}})
            }
            else if(review == 3){
                arr.push({average:{$gte:3,$lt:4}})
            }
            else if(review == 4){
                arr.push({average:{$gte:4,$lt:5}})
            }
            else if(review == 5){
                arr.push({average:5})
            }
        }


        if (keyword) {
            arr.push({ "name": { "$regex": keyword, "$options": 'i' } })
        }
        else {
            arr.push({ "_id": { "$exists": true } })
        }
        if (license_id) {
            arr.push({ "userLicense.licenses._id": ObjectId(license_id) })
        }

        if (insurance_id) {
            arr.push({ "userInsurance.insurances._id": ObjectId(insurance_id) })
        }

        if (state_id) {
            arr.push({ "userState.states._id": ObjectId(state_id) })
        }
        if (language_id) {
            arr.push({ "userLanguage.languages._id": ObjectId(language_id) })
        }

        wrkObj = { $and: arr }


        let start = parseInt(page)
        let getLimit = parseInt(limit)



        let aggregationCriteria = [
            { $match: match },
            {
                "$lookup": {
                    from: "userinsurances",
                    localField: "_id",
                    foreignField: "user_id",
                    as: "userInsurance"
                }
            },
            { "$lookup": { from: "insurances", localField: "userInsurance.insurance_id", foreignField: "_id", as: "userInsurance.insurances" } },

            { "$lookup": { from: "userlicenses", localField: "_id", foreignField: "user_id", as: "userLicense" } },
            { "$lookup": { from: "licenses", localField: "userLicense.license_id", foreignField: "_id", as: "userLicense.licenses" } },

            { "$lookup": { from: "userstates", localField: "_id", foreignField: "user_id", as: "userState" } },
            { "$lookup": { from: "states", localField: "userState.state_id", foreignField: "_id", as: "userState.states" } },

            { "$lookup": { from: "userlanguages", localField: "_id", foreignField: "user_id", as: "userLanguage" } },
            { "$lookup": { from: "languages", localField: "userLanguage.language_id", foreignField: "_id", as: "userLanguage.languages" } },

            { "$lookup": { from: "userreviews", localField: "_id", foreignField: "user_id", as: "userReview" } },
            { $sort: { "userreviews.updatedAt": -1 } },

            { "$match": wrkObj },

            {
                "$project": {
                    "_id": 1, "bio": 1, "name": 1, "image": 1, "email": 1, "is_verified": 1, "business_years": 1, "agency": 1,"average":1,
                    "insurances": "$userInsurance.insurances",
                    "licenses": "$userLicense.licenses",
                    "states": "$userState.states",
                    "languages": "$userLanguage.languages",
                    "review": "$userReview"

                }
            },
            { $sort: { is_verified: -1,_id:-1 } },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            

        ]
        console.log(aggregationCriteria)
        let user = await userSchema.aggregate(aggregationCriteria);
        let agent = []
        let getcount = 0;

        if (user[0].data.length > 0) {

            // if(user[0].data.review.length > 0){
            //     user[0].data.review = user[0].data.review[0]
            // }
            await Promise.all(user[0].data.map(async x => {

                let average = await ReviewModal.aggregate([
                    {
                        $match: { user_id: ObjectId(x._id), rating: { $ne: 0 } }
                    },
                    {
                        $group:
                        {
                            _id: "null",
                            averagetet: { $avg: "$rating" }
                        }
                    }
                ]);
                x.averageRating = 0
                if (average.length > 0) {
                    x.averageRating = average[0].averagetet
                }
                if (x.review.length > 0) {
                    x.review = x.review[0]
                }


            })
            )
            agent = user[0].data
            getcount = user[0].metadata[0].total

        }
        return ({ count: getcount, agent })
    }

    /*==========Customer Booking module=========*/
    static async addBooking(agent_id, first_name, last_name, email, dob, address, apartment, city, zipcode, phone_number, date, time, booking_id, insurance_type, state) {
        let userDetail = await userSchema.findOne({ "_id": agent_id,"is_deactivated":0 });
        let insurance = await InsuranceModel.findOne({"_id":insurance_type})
        if(userDetail != null){
            if (userDetail.subscription_status != 1) {
                if (booking_id) {
                    const getBooking = {}
                    getBooking.dob = dob
                    getBooking.address = address
                    getBooking.apartment = apartment
                    getBooking.city = city
                    getBooking.state = state
                    getBooking.zipcode = zipcode
                    getBooking.phone_number = phone_number
                    getBooking.insurance_type = insurance.name
                    getBooking.first_name = first_name
                    getBooking.last_name = last_name
                    getBooking.email = email
                    getBooking.date = date
                    getBooking.time = time
                    await CustomerBookingModal.findOneAndUpdate({ "_id": booking_id }, { $set: getBooking }, { new: true, upset: true });
                    //email//
                    let subject = "You're all set!"
                    let getemail = userDetail.email
                    let getAgentName = userDetail.name
                    let getSubscribes = await UnsubscribeModal.findOne({ "email": getemail });
                    if(getSubscribes == null){
                        let getUrl = process.env.URL
                        await Common.booking(getemail, subject, getAgentName, time,date,getUrl);
                        await Common.booking(email, subject, getAgentName, time,date,getUrl);
                    }
    
                    let title = "Appointment"
                    let notification = "Appointment booked"
                    let type = "appointment"
                    await this.addNotification(title,notification,type,agent_id)
                    
                    return ({ "message": "Booking added sucessfully" })
                }
                else {
                    const booking = new CustomerBookingModal({ "user_id": agent_id, "first_name": first_name, "last_name": last_name, "email": email, "status": 2, "date": date, "time": time });
                    const data = await booking.save();
                    let bookingId = data._id
                    return ({ "booking_id": bookingId })
                }
    
            }
            else {
                throw ("Sorry, but you cannot book your own calendar.")
            }
        }
        else{
            throw ("Sorry, agent deactivated")
        }
        

    }
    static async scheduleCall(agent_id, first_name, last_name, email, phone_number, insurance_type, message) {
        let userDetail = await userSchema.findOne({ "_id": agent_id });
        if (userDetail.subscription_status == 1) {
            const booking = new CustomerBookingModal({ "user_id": agent_id, "first_name": first_name, "last_name": last_name, "email": email, "status": 1, "phone_number": phone_number, "insurance_type": insurance_type, "message": message });
            const data = await booking.save();
            return ({ "message": "Booking added sucessfully" })
        }
        else {
            throw ("agent donot have access for this booking")
        }
    }
    static async deleteCard(agent_id) {
        let userDetail = await userSchema.findOne({ "_id": agent_id });
        if (userDetail != null) {
            let subsData = {}
            let userData = {}
            subsData.exp_month = 0
            subsData.exp_year = 0
            subsData.brand = ""
            subsData.card_number = 0
            userData.card_attach = 0
            await UserSubscriptionModal.findOneAndUpdate({ "user_id": agent_id }, { $set: subsData }, { new: true, upset: true });

            await userSchema.findOneAndUpdate({ "_id": agent_id }, { $set: userData }, { new: true, upset: true });
        }
        let user = await this.getUserProfile(agent_id)
        return ({ "message": "Card deleted", ...user })

    }
    static async getBookingsList(page, limit, agent_id,type,keyword) {
        let start = parseInt(page)
        let getLimit = parseInt(limit)
        let startDate = new Date();
        var lastWeekStart = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
        console.log(lastWeekStart)
        let arr = []
        let wrkObj = {}
        arr.push({ user_id: ObjectId(agent_id) })
        if(keyword){
            arr.push(
                {"$or":
                    [
                        {"first_name":{$regex:keyword}},
                        {"last_name":{$regex:keyword}},
                        {"email":{$regex:keyword}}
                        
                    ]
                }
            )
        }
        if (type == 'today') {
            startDate.setHours(0,0,0,0);
            arr.push({ date:startDate })
        }
        else if(type == 'month'){
            arr.push({
                $expr: {
                    $eq: [{ $month: '$date' }, { $month: new Date() }],
                }
            })
        }
        else if(type == 'year'){
            arr.push({
                $expr: {
                    $eq: [{ $year: '$date' }, { $year: new Date() }],
                }
            })
        }
        else if(type == 'week'){
            arr.push({
                $expr: {
                    $eq: [{ $week: '$date' }, { $week: new Date() }],
                }
            })
        }
        console.log(arr)

        wrkObj = { $and: arr }
         
        let getBooking = await CustomerBookingModal.aggregate([
            {
                "$match": wrkObj ,
            },
            {
                '$facet': {
                    metadata: [{ $count: "total" }],
                    data: [{ $skip: start * getLimit }, { $limit: getLimit }] // add projection here wish you re-shape the docs
                }
            },
            { $sort: { _id: -1 } }
        ]);
        let bookings = []
        let count = 0
        if (getBooking[0].data.length > 0) {
            bookings = getBooking[0].data
            count = getBooking[0].metadata[0].total
        }
        return ({ count: count, booking: bookings })
    }
    static async blockDates(user_id, date) {
        let getBlockData = await BlockDatesModal.findOne({ "user_id": user_id,"date":date });
        if(getBlockData == null){
            const block = new BlockDatesModal({ "user_id": user_id, "date": date });
            const data = await block.save();
            //let bookingId = data._id
            let user = await this.getUserProfile(user_id)
            return ({ "message": "Date blocked", ...user })
        }
        else{
            throw "Date already blocked"
        }
        
    }
    static async unblockDates(user_id, date) {
        await BlockDatesModal.deleteOne({ user_id: user_id,date:date });
        
        //let bookingId = data._id
        let user = await this.getUserProfile(user_id)
        return ({ "message": "Date unblocked", ...user })
    }
    static async unsubscribe(email) {
        let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
        if(getSubscribe != null){
            return ({ "message": "Already unsubscribed"})
        }
        else{
            const unsubscribe = new UnsubscribeModal({ email});
            await unsubscribe.save();
            return ({ "message": "Email unsubscribed"})
        }
    }
    static async cancelAppointment(booking_id){
        let subsData = {}
        subsData.booking_status = 2
        await CustomerBookingModal.findOneAndUpdate({ "_id": booking_id }, { $set: subsData }, { new: true, upset: true });

        let customerBookingDetail = await CustomerBookingModal.findOne({ "_id": booking_id });
        let userDetail = await userSchema.findOne({ "_id": customerBookingDetail.user_id });

        //email//
        if(customerBookingDetail.status == 2){
            let subject = 'Appointment Cancelled'
            let email = userDetail.email
            let customerName = customerBookingDetail.first_name
            let getdate = customerBookingDetail.date
            console.log(getdate)
            var dates = getdate.getFullYear()+'-'+("0" + (getdate.getMonth() + 1)).slice(-2)+'-'+("0" + getdate.getDate()).slice(-2)
            let time = customerBookingDetail.time

            let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
            if(getSubscribe == null){
                let getUrl = process.env.URL
                await Common.cancelAppointment(email, subject, customerName, dates,time,getUrl);
            }
        }
        return ({ "message": "Appointment canceled successfully"})
    }
    static async inviteUser(token,email){
        let reviewDetail = await ReviewModal.findOne({ "invite_token": token });
        if(reviewDetail != null){
            let userDetail = await userSchema.findOne({ "_id": reviewDetail.user_id });
            let data = {}
            data.invite_token = ""
            await ReviewModal.findOneAndUpdate({ "invite_token": token }, { $set: data }, { new: true, upset: true });
            
            Promise.all(email.map(async x => {
                
                let customerName = reviewDetail.name
                let agentName = userDetail.name
                let agent_id = userDetail._id
                
                //email//
                let subject = customerName+' shared a contact with you...'
                let getSubscribe = await UnsubscribeModal.findOne({ "email": email });
                if(getSubscribe == null){
                    let getUrl = process.env.URL
                    await Common.inviteEmail(email, subject, customerName, agentName,agent_id,getUrl);
                }
            }))
            return { message: "Email sent successfully"}
        }
        else{
            throw "Invalid token"
        }
    }
    static async notifications(user_id,page,limit){
        let notifications = await NotificationSchema.find({ "user_id": user_id }).skip(page).limit(limit);
        let notificationCount = await NotificationSchema.find({ "user_id": user_id }).count();
        return ({count:notificationCount,notifications})
    }
    static async notificationDelete(notification_id){
        await NotificationSchema.deleteOne({ "_id": notification_id });
        return ({message:"Notification deleted"})
    }

    static async getUWappAgentPortal(user_id){
        console.log('------------',user_id)
        // let agentPortals = await UWappAgentPortalModel.find({'user_id':user_id}).populate("uwappMedicalHistoryItem");
        let agentPortals = await UWappAgentPortalModel.aggregate([
            {
                $match: { user_id: ObjectId(user_id)}
            },
            { "$lookup": { from: "uwappmedicalhistory", localField: "_id", foreignField: "uwappAgent_id", as: "medicalItems" } },
            { "$lookup": { from: "uwapppersonalhistory", localField: "_id", foreignField: "uwappAgent_id", as: "personalItems" } },
            {
                "$project": {
                    "_id": 1, "birthday": 1,"phoneNumber":1, "gender": 1, "nicotineStatus": 1, "coverageAmount": 1, "selectedState": 1, "height": 1, "weight": 1, "appType": 1, "agency": 1, "firstName": 1, "lastName": 1, "email": 1, "insuranceStatus": 1,
                    "selectedMedicalItems": "$medicalItems",
                    "selectedPersonalItems": "$personalItems",
                }
            }
        ]);
        return agentPortals
    }

    static async addUWappAgentPortal(
        user_id,
        birthday,
        phoneNumber,
        gender,
        nicotineStatus,
        coverageAmount,
        selectedState,
        height,
        weight,
        medicalHistory,
        personalHistory,
        appType,
        firstName,
        lastName,
        email,
        insuranceStatus,
    ){
        const uwappAgent = {}
        uwappAgent.user_id = user_id
        uwappAgent.birthday = birthday
        uwappAgent.phoneNumber = phoneNumber
        uwappAgent.gender = gender
        uwappAgent.nicotineStatus = nicotineStatus
        uwappAgent.coverageAmount = coverageAmount
        uwappAgent.selectedState = selectedState
        uwappAgent.height = height
        uwappAgent.weight = weight
        uwappAgent.appType = appType
        uwappAgent.firstName = firstName
        uwappAgent.lastName = lastName 
        uwappAgent.email = email 
        uwappAgent.insuranceStatus = insuranceStatus 
        const data = new UWappAgentPortalModel(uwappAgent)
        await data.save()
        if(medicalHistory.length != 0){
            medicalHistory.forEach(async x => {
                let medicalHistoryItem = x
                medicalHistoryItem.uwappAgent_id = data._id
                const medicalData = new UwappMedicalHistoryModel({
                    'uwappAgent_id':medicalHistoryItem.uwappAgent_id,
                    'title':medicalHistoryItem.title,
                    'status':medicalHistoryItem.status,
                    'qcat':medicalHistoryItem.qcat,
                    'msg':medicalHistoryItem.msg,
                    'qtype':medicalHistoryItem.qtype,
                    'ptype':medicalHistoryItem.ptype,
                    '__v':medicalHistoryItem.__v,
                    'answer':medicalHistoryItem.answer
                })
                await medicalData.save()
            })
        }
        if(personalHistory.length != 0){
            personalHistory.forEach(async x => {
                let personalHistoryItem = x
                personalHistoryItem.uwappAgent_id = data._id
                const personalData = new UwappPersonalHistoryModel({
                    'uwappAgent_id':personalHistoryItem.uwappAgent_id,
                    'title':personalHistoryItem.title,
                    'status':personalHistoryItem.status,
                    'qcat':personalHistoryItem.qcat,
                    'msg':personalHistoryItem.msg,
                    'qtype':personalHistoryItem.qtype,
                    'ptype':personalHistoryItem.ptype,
                    '__v':personalHistoryItem.__v,
                    'answer':personalHistoryItem.answer
                })
                await personalData.save()
            })
        }
        return data;
    }
}

module.exports = UserService;