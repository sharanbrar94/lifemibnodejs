require("dotenv").config();
const jwt = require("jsonwebtoken");
const UserService = require("./users.service");
const logger = require('../../logger')

class UserController {

    /*======signup module======*/
    static async signup(req, res, next) {
        try {
            const {
                email,
                password
            } = req.body;

            const result = await UserService.signup(email, password);
            return res.status(200).json(result);
        } catch (err) {
            console.log("erro::", err);

            return res.status(400).json({
                error: "bad_request",
                error_description: err,
            });

        }
    }

    /*======login module=====*/
    static async login(req, res) {
        try {
            const {
                email,
                password,
                fcm_id,
                device_id,
                device_type
            } = req.body;
            let user = await UserService.login({
                email,
                password,
                fcm_id,
                device_id,
                device_type,
            });
            return res.status(200).json(user);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    /*======static data=====*/
    static async staticData(req, res) {
        try {
            let data = {}
            data.languages = await UserService.getLanguages();
            data.licenses = await UserService.getLicenses();
            data.states = await UserService.getStates();
            data.insurances = await UserService.getInsurances();
            data.subscription = await UserService.getSubscription();
            return res.status(200).json(data);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    /*=======profile module=====*/
    static async getUserProfile(req, res, next) {
        try {
            let user_id = "";
            if (req.query.id) {
                user_id = req.query.id
            } else {
                user_id = req.user._id
            }
            let user = await UserService.getUserProfile(user_id);
            return res.status(200).json(user);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async updateUserProfile(req, res, next) {
        try {
            let user_id = req.user._id
            const {
                name,
                phone,
                agency,
                country_code,
                image,
                languages,
                producer_number,
                business_years,
                insurances,
                licenses,
                sp_term_providers,
                sp_whole_providers,
                states,
                subscription_id,
                stripe_token,
                subscription_email,
                subscription_name,
                email_content,
                bio
            } = req.body;
            let result = await UserService.updateUserProfile(user_id, name, phone, agency, country_code, image, languages, producer_number, business_years, insurances, licenses, sp_term_providers, sp_whole_providers, states, subscription_id, stripe_token, subscription_email, subscription_name, email_content, bio);
            return res.status(200).json(result);
        } catch (err) {
            console.log(err);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }

    /*=======Change password=======*/
    static async changePassword(req, res, next) {
        try {

            let user_id = req.user._id
            const {
                new_password,
                old_password
            } = req.body;
            const result = await UserService.changePassword(user_id, new_password, old_password);
            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    /*=======Deactivate account======*/
    static async deactivateAccount(req, res, next) {
        try {
            let user_id = req.user._id
            const {
                deactivate_reason
            } = req.body;
            const result = await UserService.deactivateAccount(user_id, deactivate_reason);
            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    /*======Forgot password======*/
    static async forgotPassword(req, res, next) {
        try {
            const email = req.body.email;
            const result = await UserService.forgotPassword(email);
            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err,
            });
        }
    }
    static async checkEmailOtp(req, res, next) {
        try {
            let otp = req.body.otp;
            const result = await UserService.checkEmailOtp(otp);
            return res.status(200).json(result)
        } catch (err) {
            return res.status(400).json({
                'error': "bad_request",
                'error_description': err
            });
        }
    }
    static async resetPassword(req, res, next) {
        try {
            const id = req.user._id
            const {
                password
            } = req.body;
            let result = await UserService.resetPassword(id, password);
            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err,
            });
        }
    }





    static async resendEmail(req, res) {
        try {
            const {
                email,
                type,
                name
            } = req.query;

            let result = await UserService.resendEmail(email, type, name);
            res
                .status(200)
                .json({
                    message: "We just sent you email, please check."
                });
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }








    static async logout(req, res, next) {
        try {
            let fcm_id = req.body.fcm_id;
            console.log("fcm_id", fcm_id);
            await UserService.logout(fcm_id);
            return res
                .status(200)
                .json({
                    message: "You are logged out successfully!"
                });
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }

    /*=======calendar module=========*/
    static async addCalendar(req, res, next) {
        try {
            let user_id = req.user._id
            let timezone = req.body.timezone
            let duration = req.body.duration
            let timeslot = req.body.timeslot
            let result = await UserService.addCalendar(user_id, timezone, duration, timeslot);
            return res.status(200).json(result);
        } catch (err) {
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }
    static async editCalendar(req, res, next) {
        try {
            let calendar_id = req.body.calendar_id
            let timezone = req.body.timezone
            let duration = req.body.duration
            let timeslot = req.body.timeslot
            let result = await UserService.editCalendar(calendar_id, timezone, duration, timeslot);
            return res.status(200).json(result);
        } catch (err) {
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }
    static async getCalendar(req, res, next) {
        try {
            let user_id = req.user._id
            let result = await UserService.getCalendar(user_id);
            return res.status(200).json(result);
        } catch (err) {
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }

    /*======Review module========*/
    static async reviewRequest(req, res, next) {
        try {
            let user_id = req.user._id
            let email = req.body.email

            // let checkReview = await UserService.checkReviewEmail(email);

            // if (!checkReview.emails.length) {
            //     return res
            //     .status(400)
            //     .json({
            //         error: "duplicate_review",
            //         error_description: 'Review has been already requested from same users'
            //     });
            // } else {
                let result = await UserService.reviewRequest(user_id, email);
                return res.status(200).json(result);

            //}

        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }
    static async addReview(req, res, next) {
        try {
            let token = req.body.token
            let rating = req.body.rating
            let review = req.body.review
            let name = req.body.name
            let icon = req.body.icon
            let images = req.body.images
            let result = await UserService.addReview(token, rating, review, name, icon, images);
            return res.status(200).json(result);
        } catch (err) {
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }
    static async getReviews(req, res, next) {
        try {
            let page = req.query.page
            let limit = req.query.limit
            if (req.query.id) {
                var user_id = req.query.id
            } else {
                var user_id = req.user._id
            }
            let result = await UserService.getReview(user_id, page, limit);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }
    static async getAgentDetail(req, res, next) {
        try {
            let token = req.query.token
            let result = await UserService.getAgentDetail(token);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }

    /*======search agent======*/
    static async searchAgent(req, res, next) {
        try {
            let pincode = req.query.pincode
            let page = req.query.page
            let limit = req.query.limit
            let keyword = req.query.keyword
            let license_id = req.query.license_id
            let insurance_id = req.query.insurance_id
            let state_id = req.query.state_id
            let language_id = req.query.language_id
            let review = req.query.review
            let result = await UserService.searchAgent(page, limit, pincode, keyword, license_id, insurance_id, state_id, language_id, review);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res
                .status(400)
                .json({
                    error: "bad_request",
                    error_description: err
                });
        }
    }

    /*==========Customer Booking module=========*/
    static async addBooking(req, res, next) {
        try {
            let agent_id = req.body.agent_id
            let first_name = req.body.first_name
            let last_name = req.body.last_name
            let email = req.body.email
            let dob = req.body.dob
            let address = req.body.address
            let apartment = req.body.apartment
            let city = req.body.city
            let zipcode = req.body.zipcode
            let phone_number = req.body.phone_number
            let date = req.body.date
            let time = req.body.time
            let booking_id = req.body.booking_id
            let insurance_type = req.body.insurance_type
            let state = req.body.state

            let result = await UserService.addBooking(agent_id, first_name, last_name, email, dob, address, apartment, city, zipcode, phone_number, date, time, booking_id, insurance_type, state);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async scheduleCall(req, res, next) {
        try {
            let agent_id = req.body.agent_id
            let first_name = req.body.first_name
            let last_name = req.body.last_name
            let email = req.body.email
            let phone_number = req.body.phone_number
            let insurance_type = req.body.insurance_type
            let message = req.body.message

            let result = await UserService.scheduleCall(agent_id,first_name,last_name,email,phone_number,insurance_type,message);

            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async deleteCard(req, res, next) {
        try {
            let agent_id = req.user._id
            let result = await UserService.deleteCard(agent_id);
            return res.status(200).json(result);
        } catch (err) {
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async getBookingsList(req, res, next) {
        try {
            let page = req.query.page
            let limit = req.query.limit
            let agent_id = req.user._id
            let type = req.query.type
            let keyword = req.query.keyword
            let result = await UserService.getBookingsList(page, limit, agent_id,type,keyword);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async blockDates(req, res, next) {
        try {
            let user_id = req.user._id
            let date = req.body.date
            let result = await UserService.blockDates(user_id, date);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async unblockDates(req, res, next) {
        try {
            let user_id = req.user._id
            let date = req.body.date
            let result = await UserService.unblockDates(user_id, date);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async unsubscribe(req, res, next){
        try {
            let email = req.body.email
            let result = await UserService.unsubscribe(email);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async cancelAppointment(req, res, next){
        try {
            let booking_id = req.body.booking_id
            let result = await UserService.cancelAppointment(booking_id);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async inviteUser(req, res, next){
        try {
            let token = req.body.token
            let email = req.body.email
            console.log("token",token)
            console.log("email",email)
            let result = await UserService.inviteUser(token,email);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async notifications(req, res, next){
        try {
            let user_id = req.user._id
            let page = req.body.page
            let limit = req.body.limit
            let result = await UserService.notifications(user_id,page,limit);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }
    static async notificationDelete(req, res, next){
        try {
            let notification_id = req.body.notification_id
            let result = await UserService.notificationDelete(notification_id);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    static async getUWappAgentPortal(req, res, next){
        try {

            console.log('=====user_id====',req.user._id)
           let user_id = req.user._id
           let result = await UserService.getUWappAgentPortal(user_id);
           return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    static async addUWappAgentPortal(req, res, next){
        try {
            let user_id = req.user._id
            let birthday = req.body.birthday
            let phoneNumber = req.body.phoneNumber
            let gender = req.body.gender
            let nicotineStatus = req.body.nicotineStatus
            let coverageAmount = req.body.coverageAmount
            let selectedState = req.body.selectedState
            let height = req.body.height
            let weight = req.body.weight
            let medicalHistory = req.body.selectedMedicalHistoryItems
            let personalHistory = req.body.selectedPersonalHistoryItems
            let medicationArr = req.body.medicationArr
            let appType = req.body.appType
            let firstName = req.body.firstName
            let lastName = req.body.lastName
            let email = req.body.email
            let insuranceStatus = req.body.insuranceStatus
            let result = await UserService.addUWappAgentPortal(
                user_id,
                birthday,
                phoneNumber,
                gender,
                nicotineStatus,
                coverageAmount,
                selectedState,
                height,
                weight,
                medicalHistory,
                personalHistory,
                medicationArr,
                appType,
                firstName,
                lastName,
                email,
                insuranceStatus,
            );
            return res.status(201).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

    static async removeUWappAgentPortal(req, res, next){
        try {
            let uwappAgent_id = req.body._id
            let result = await UserService.removeUWappAgentPortal(uwappAgent_id);
            return res.status(200).json(result);
        } catch (err) {
            logger.error(`message - ${err.message}, stack trace - ${err.stack}`);
            return res.status(400).json({
                error: "bad_request",
                error_description: err
            });
        }
    }

}

module.exports = UserController;