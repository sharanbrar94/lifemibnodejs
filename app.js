require('dotenv').config();
require('./connection');
const swaggerUi = require("swagger-ui-express");
const express = require('express');
var cors = require("cors");
var http = require("http");
var https = require("https");
var fs = require("fs");
const app = express();
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept,Authorization");
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE,PATCH,GET ');
        return res.status(200).json({});
    }
    next();
});

app.use(express.json()); //middleware to parse json
const userRoutes = require("./src/users/users.router");
const userSwagger = require("./src/users/users.swagger.json");
const adminRoutes = require("./src/admin/admin.routes");
const adminSwagger = require("./src/admin/admin.swagger.json");
const uploadRoutes = require("./src/upload/upload.routes");
const uploadSwagger = require("./src/upload/upload.swagger.json");
var swaggermerge = require("swagger-merge");
var info = {
    version: "0.0.1",
    title: "Life-mib",
    description: "Life-mib apis",
};

if (process.env.SSL == "true") {
    var scheme = ["https"];
} else {
    var scheme = ["http"];
}
console.log(scheme)

merged = swaggermerge.merge(
    [
        userSwagger,adminSwagger,uploadSwagger
    ],
    info,
    "/api",
    process.env.DOMAIN + ":" + process.env.PORT,
    scheme
);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(merged));

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});
app.use("/api",userRoutes);
app.use("/api/admin", adminRoutes);
app.use("/api", uploadRoutes);
app.use(cors({origin: '*'}));

// const api = require('./api/api');//route to API's

// app.use('/', api);


const port = 3000 || process.env.PORT;

if (process.env.SSL == "true") {
    var server = https.createServer(
      {
        key: fs.readFileSync(process.env.SSL_KEY),
        cert: fs.readFileSync(process.env.SSL_CERT),
      },
      app
    );
  } else {
    var server = http.createServer(app);
}

server.listen(port, console.log(`server is running on PORT ${port}`));