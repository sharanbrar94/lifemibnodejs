// const router = require('../api/api');
// const userSchema = require('../model/tasks');

// //adding tasks 
// exports.addTask = async(req,res)=>{
//     try{
//         const {task , assignedBy , assignedTo } = req.body;
//         const newtask = new userSchema({task , assignedBy , assignedTo});
//         if(task){
//             const data = await newtask.save();
//             return res.json({message : req.body}); 
//         }
//     }catch(err){
//         console.log("Error in adding tasks " + err);
//     }
// }

// //updating particular task
// exports.updateTask = async(req,res)=>{
//     try{
//         const taskID = await userSchema.findById(req.params.id);
//         taskID.task = req.body.task,
//         taskID.assignedBy = req.body.assignedBy;
//         taskID.assignedTo = req.body.assignedTo;
//         const updatedTask = await taskID.save();
//         res.status(200).json({message : "task updated successfully"});
//     }catch(err){
//         console.log("Error in updating task " + err);
//     }
// }

// //fetching all the tasks
// exports.getAllTasks = async(req,res)=>{
//     try{
//         const tasks = await userSchema.find();
//         res.status(200).json({Tasks : tasks});
//     }catch(err){
//         console.log("Error in fetching all the tasks " + err);
//     }
// }

// //deleting particular task
// exports.deleteTask = async(req,res)=>{
//     try{
//         await userSchema.deleteOne({_id : req.params.id } , (err , result)=>{
//             if(err){
//                 return res.status(400).json({error : err});
//             }else{
//                 return res.status(200).json({
//                     message : "one task deleted successfully..."
//                 })
//             }
//         })
//     }catch(err){
//         console.log("Error in deletion " + err);
//     }
// }


// //get recent tasks
// exports.getRecentTasks = async(req,res)=>{
//     try{
//         let recentTasks = [];
//         const data = await userSchema.find();
//         let todayDate = Date().split(' '); // get today's date
//         let todayDatee = todayDate[1]+ " " + todayDate[2]+ " " + todayDate[3];

//         for(let i = 0; i < data.length ; i++){
//             let info = data[i].createdAt; // get date on which document created
//             let currentDate = String(info).split(' ');
//             let currentDatee = currentDate[1]+ " " + currentDate[2]+ " " + currentDate[3];

//             if(currentDatee == todayDatee){
//                 recentTasks.push(data[i])
//             }
//         }
//         if(recentTasks.length != 0){
//             res.status(200).json({message : recentTasks});
//         }else{
//             res.status(200).json({message : "There are no recent tasks"});
//         }
//     }catch(err){
//         throw err;
//     }
// }

// //get earlier tasks
// exports.getEarlierTasks = async(req,res)=>{
//     try{
//         let earlierTasks = [];
//         const data = await userSchema.find();
//         let todayDate = Date().split(' '); // get today's date
//         let todayDatee = todayDate[1]+ " " + todayDate[2]+ " " + todayDate[3];

//         for(let i = 0; i < data.length ; i++){
//             let info = data[i].createdAt; // get date on which document created
//             let currentDate = String(info).split(' ');
//             let currentDatee = currentDate[1]+ " " + currentDate[2]+ " " + currentDate[3];

//             if(currentDatee != todayDatee){
//                 earlierTasks.push(data[i])
//             }
//         }

//         if(earlierTasks.length != 0){
//             res.status(200).json({message : earlierTasks});
//         }else{
//             res.status(200).json({message : "There are no earlier tasks"});
//         }
//     }catch(err){
//         throw err;
//     }
// }